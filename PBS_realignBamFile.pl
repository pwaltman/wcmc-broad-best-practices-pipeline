#!/bin/env perl
use lib "/home/waltmape/elementoLabSoftware/PERL_MODULES";
use strict;
use Sets;
use PBS;
use Getopt::Long;
use Switch;
use File::Basename;
use Data::Dumper;
use POSIX qw/ceil/;
use List::Util qw[min max];

if (@ARGV == 0) {
  print "Args: --bamFile=FILE --bamidxFile=FILE --bwaidx=FILE --submit=INT\n";
  exit;
}

my $submit = 2;
my $localdatadir = "$ENV{PWD}";
my $USER="$ENV{USER}";
my $projectRootDir="FAHNSC";
my $serapeum=undef;
my $serapeum_path=undef;
# this variable allows users to manually specify the $serapeum_path, while the script will determine where to store the result based on the $localdatadir var
my $serapeum_root_dir="oelab_scratch008";
my $serapeum_outpath=undef;  
my $bamFile = undef;
my $bamidxFile = undef;
my $targetFile = undef;
my $numcpus = 16;
my $cpuMem  = 3;
my $walltime = "36:00:00";
my $after    = undef;
my $target   = undef;
my $debug = 0;
my $gatk = "/home/waltmape/bin/GenomeAnalysisTK.jar";
my $prefix = "test_devel";
my $runtype = "BWAMEM_GATK";
my $gatk_resource_dir="$ENV{HOME}/FAHNSC/resources/grch37/gatk";
my $gatk_resource_ver="b37";
## now set vars for the necessary resource files
my $onekg_indels="$gatk_resource_dir/$gatk_resource_ver/1000G_phase1.indels.${gatk_resource_ver}.vcf";
my $mills_indels="$gatk_resource_dir/$gatk_resource_ver/Mills_and_1000G_gold_standard.indels.${gatk_resource_ver}.vcf";
my $bwaidx = "$ENV{HOME}/FAHNSC/resources/grch37/indices/bwa/broad/Homo_sapiens_assembly19.fasta";
my $bwaidx_orig=$bwaidx;  
my $bwapath = dirname( $bwaidx );

GetOptions("submit=s"            => \$submit,
	   "localdatadir=s"      => \$localdatadir,
	   "projectRootDir=s"    => \$projectRootDir,
	   "serapeum=s"          => \$serapeum,
	   "serapeum_path=s"     => \$serapeum_path,
           "bamFile=s"           => \$bamFile,
           "bamidxFile=s"        => \$bamidxFile,
	   "numcpus=s"           => \$numcpus,
           "targetFile=s"        => \$targetFile,
	   "cpuMem=s"            => \$cpuMem,
           "walltime=s"          => \$walltime,
           "after=s"             => \$after,
	   "debug=s"             => \$debug,
	   "target=s"            => \$target,
	   "gatk=s"              => \$gatk,
           "prefix=s"            => \$prefix,
           "runtype=s"           => \$runtype,  
	   "gatk_resource_dir=s" => \$gatk_resource_dir,
	   "gatk_resource_ver=s" => \$gatk_resource_ver,
	   "bwaidx=s"            => \$bwaidx);

die( "No bam file provided for parameter --bamFile" ) if not defined $bamFile;
die( "No bam index file provided for parameter --bamidxFile" ) if not defined $bamidxFile;
die( "No target file provided for parameter --targetFile" ) if not defined $targetFile;
die( "per process memory must be specified in terms of G (with or without the 'G'), NOT megabytes") if $cpuMem =~ /[mbMB]/ ; ## check if cpuMem is specified in terms megabytes
if ( $cpuMem =~ /gG/ ) {
    $cpuMem =~ s/gG//;
}
my $totalMem = ($numcpus * 2) + 8;
die( "per total memory of job (# cpus * memory_per_cpu) must be less than 256G; current total: $totalMem") if $totalMem > 256;

my $jobMem = ceil($totalMem / $numcpus);
if (! -e $gatk    ) {
  die("GATK jar file: $gatk does not exist.\n");
}


my $local = 0;
if ( $submit == 3 ) {
  $local = 1;
}


sub get_serapeum_path {
    my ($lclDataDir, $projRootDir) = @_;
    my @subcomps= split( /$projRootDir/, $lclDataDir );
    my $retval = "$projRootDir/$subcomps[1]";
    ## drop any double slashes - just in case
    $retval =~ s/\/\//\//g;
    return $retval; 
}


sub lftp_filechk {
    my ($user, $fname, $serapeumDir, $serapeumServ ) = @_;
    chomp( my $filechk=`echo \"ls $serapeumDir/$fname\" | sftp -b - ${user}\@${serapeumServ}.qib.pbtech 2>\&1 > /dev/null | grep -i \"not found\" | wc -l`) ;
    if ( $filechk ) {
	return 0;
    } else {
	return 1;
    }
}

sub get_serapeum_serv {
    my ($serapeumDir, $fname) = @_;
    my $retval=undef;
    my @servers = ( "serapeum1", "serapeum2" );

    foreach my $serv (@servers) {
	if ( &lftp_filechk( $USER,
			    $fname,
			    $serapeumDir,
			    $serv ) ) {
	    $retval = $serv;
	}
    }
    return $retval; 
}

if (! -e $bamFile) {
    $serapeum_outpath=&get_serapeum_path( $localdatadir, $projectRootDir ); # if ! defined $serapeum_path;
    $serapeum_path=&get_serapeum_path( $localdatadir, $projectRootDir ) if ! defined $serapeum_path;
    $serapeum=&get_serapeum_serv( $serapeum_path, $bamFile ) if ! defined $serapeum;
    if ( defined $serapeum ) {
	if ( $serapeum eq "serapeum1" ) {
	    $serapeum_root_dir="oelab_scratch008";
	} else {
	    $serapeum_root_dir="melnick-elemento_scratch001";
	}
    } else {
	die("$bamFile does not exist.\n");
    }
}

if (! -e $bamidxFile) {
    if ( defined $serapeum ) {
	## assume that everything was set while checking the location of the $bamFile AND that $bamidsFile is in the same dir
	## so, just check the location of the bamidxFile, using the vals set above
	my $dbg = &lftp_filechk( $USER,
				 $bamidxFile,
				 $serapeum_path,
				 $serapeum );
	print "dbg: $dbg\n";
	if ( ! &lftp_filechk( $USER,
			      $bamidxFile,
			      $serapeum_path,
			      $serapeum ) ) {
	    die("$bamidxFile does not exist.\n");
	}
    }
}

if (! -e $targetFile) {
    if ( defined $serapeum ) {
	## assume that everything was set while checking the location of the $bamFile AND that $bamidsFile is in the same dir
	## so, just check the location of the bamidxFile, using the vals set above
	if ( ! &lftp_filechk( $USER,
			      $targetFile,
			      $serapeum_path,
			      $serapeum ) ) {
	    die("$targetFile does not exist.\n");
	}
    }
}


## declare the output variables
my $outFile="realn_${bamFile}";
my $outIndex=$outFile;
$outIndex=~ s/bam$/bai/;


# legacy var namess from code copied from PBS_quickAlignPE.pl
my $outFound = 0;
my $outidxFound = 0;
if ( defined $serapeum ) {
    $outFound = &lftp_filechk( $USER, 
			       $outFile, 
			       $serapeum_outpath,
			       $serapeum);
    $outidxFound = &lftp_filechk( $USER, 
				  $outIndex,
				  $serapeum_outpath,
				  $serapeum);
} else {
    if ( -e "$localdatadir/$outFile" ) {
	$outFound = 1;
    }
    if ( -e "$localdatadir/$outIndex" ) {
	$outidxFound = 1;
    }
}


if ( $outFound && $outidxFound ) {
    ## looks like the output file exists, in which case, if this isn't a speedseq alignment, just start the next step
    chdir( $localdatadir );

    my $cmd = "PBS_genRecalTable.pl \\\n" .
	"\t--localdatadir $localdatadir \\\n" .
	"\t--bamFile $outFile \\\n" .
	"\t--bamidxFile $outIndex \\\n" .
	"\t--bwaidx $bwaidx_orig \\\n" .
	"\t--gatk_resource_dir $gatk_resource_dir \\\n" .
	"\t--gatk_resource_ver $gatk_resource_ver \\\n" .
	"\t--numcpus $numcpus \\\n" .
	"\t--cpuMem $cpuMem \\\n" .
	"\t--prefix $prefix \\\n" .
	"\t--runtype $runtype --submit $submit --after $after";
    $cmd .= " \\\n\t--serapeum $serapeum" if defined $serapeum;
    $cmd .= " \\\n\t--serapeum_path $serapeum_outpath" if defined $serapeum_outpath;
    
    
    `$cmd`;
    chdir( "$ENV{PWD}\n" );
} else {
    my $realign_bamFile = PBS->new;
    my $script = "${prefix}_${runtype}_realign_bamFile_run.sh";

    if ($local == 0 ){
	$realign_bamFile->setPlatform("panda");
	$realign_bamFile->setEmail("pwaltman\@rockefeller.edu");
	$realign_bamFile->setWallTime( $walltime );
#    $realign_bamFile->setNumCPUs( $numcpus );
	$realign_bamFile->setMemory( "${totalMem}G" );
	$realign_bamFile->setScriptName($script);
	$realign_bamFile->setName("${prefix}_${runtype}_realign_bamFile");
	$realign_bamFile->setTarget( "zenodotus" );

	if (defined($target)) {
	    $realign_bamFile->setTarget($target);
	}
    }



    ## Setting work directories
    my $execdatadir  = "\$TMPDIR";
    my $oeLabSharedDir = "/zenodotus/dat02/elemento_lab_scratch/oelab_scratch_scratch007/kwe2001";
    my $homedir = "$ENV{HOME}";
    my $localperlscriptsdir = "$oeLabSharedDir/programs/SNPseeqer";
    my $tmpdir = "$execdatadir/tmp";



    ## If the job is run locally
    if ($local == 1) {
	$localdatadir = "$ENV{PWD}"; 
	$execdatadir  = "$ENV{PWD}";
	# Actual start to the bash script
	$realign_bamFile->addCmd("cd $ENV{PWD}");
    }
    else {
	$realign_bamFile->addCmd("cd $execdatadir");
	$realign_bamFile->addCmd("echo \"localdatadir: $localdatadir\"");
	$realign_bamFile->addCmd("echo \"execdatadir: $execdatadir\"");
	$realign_bamFile->addCmd("echo \"node: \$\(uname -n\)\"");
    }
    


    #  copy it to remote 
    if ($local == 0) {
	if ( defined $serapeum ) {
	    $realign_bamFile->addCmd("curdir=\$(pwd -P)");
	    $realign_bamFile->addCmd("cd $execdatadir");
	    $realign_bamFile->addCmd("lftp -e \"get $serapeum_path/$bamFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $realign_bamFile->addCmd("lftp -e \"get $serapeum_path/$bamidxFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $realign_bamFile->addCmd("lftp -e \"get $serapeum_path/$targetFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $realign_bamFile->addCmd("cd \$curdir");
	} else {
	    $realign_bamFile->addCmd("rsync -rptgoLv $localdatadir/$bamFile $execdatadir");
	    $realign_bamFile->addCmd("rsync -rptgoLv $localdatadir/$bamidxFile $execdatadir");
	    $realign_bamFile->addCmd("rsync -rptgoLv $localdatadir/$targetFile $execdatadir");
	}
	$realign_bamFile->addCmd("rsync -rptgoLv $gatk_resource_dir/$gatk_resource_ver/ $execdatadir/gatk");
	$realign_bamFile->addCmd("rsync -rptgoLv $bwapath $execdatadir");
	$bwapath = basename( $bwapath );
	$bwaidx = basename( $bwaidx );
	$bwaidx = "$execdatadir/$bwapath/$bwaidx";
	$onekg_indels="$execdatadir/gatk/1000G_phase1.indels.${gatk_resource_ver}.vcf";
	$mills_indels="$execdatadir/gatk/Mills_and_1000G_gold_standard.indels.${gatk_resource_ver}.vcf";
    }



    ## generate realigned bamFile
    $realign_bamFile->addCmd("if [ -d $tmpdir ]; then");
    $realign_bamFile->addCmd("\t mkdir $tmpdir -p");
    $realign_bamFile->addCmd("fi");

    $realign_bamFile->addCmd("echo \"generate realigned bamFile: $outFile\"" );

    my $RATC_MEM = max( (1 + 8), ($totalMem-8));
    my $cmd = "java -Djava.io.tmpdir=$tmpdir \\\n\t";
    $cmd .= "    -Xmx${RATC_MEM}g \\\n";
    $cmd .= "    -jar $gatk \\\n";
    $cmd .= "    -T IndelRealigner \\\n";
    $cmd .= "    -R $bwaidx \\\n";
    $cmd .= "    -I $bamFile \\\n";
    $cmd .= "    -targetIntervals $targetFile \\\n";
    $cmd .= "    -known $onekg_indels \\\n";
    $cmd .= "    -known $mills_indels \\\n";
    $cmd .= "    -o $outFile";
    $realign_bamFile->addCmd( $cmd );
    

    $realign_bamFile->addCmd("if [ ! -f $outFile ]; then" );
    $realign_bamFile->addCmd("    echo \"$outFile not generated, see log for explanation\" ");
    $realign_bamFile->addCmd("    exit 1" );
    $realign_bamFile->addCmd("fi");
    
    if ($local == 0) { # get BAM and bam index files back, 
	if ( defined $serapeum ) {
	    ## make sure to check if the outpath exists
	    if ( ! &lftp_filechk( $USER,
				  "",
				  $serapeum_outpath,
				  $serapeum ) ) {
		$realign_bamFile->addCmd("lftp -e \"mkdir -p /$serapeum_root_dir/$USER/$serapeum_outpath; bye\" sftp://${serapeum}.qib.pbtech"); 
	    }
	    $realign_bamFile->addCmd("lftp -e \"put $outFile; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
	    $realign_bamFile->addCmd("lftp -e \"put $outIndex; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
	    
	}else {
	    $realign_bamFile->addCmd("rsync -av $outFile $localdatadir");
	    $realign_bamFile->addCmd("rsync -av $outIndex $localdatadir");
	}
    }



    # cleanup to do if executing distantly
    #$realign_bamFile->addCmd("rm -rf $execdatadir/$bwapath &");                      # erase genome
    $realign_bamFile->addCmd("rm -f $execdatadir/$bamFile &") if defined $bamFile;   # erase data file 1
    $realign_bamFile->addCmd("rm -f $execdatadir/$bamidxFile &") if defined $bamidxFile;   # erase data file 1
    $realign_bamFile->addCmd("rm -f $execdatadir/$outFile &") if defined $bamFile;   # erase data file 1
    $realign_bamFile->addCmd("rm -f $execdatadir/$outIndex &") if defined $bamidxFile;   # erase data file 1

   # we also should remove the targets list file as it is no longer needed/necessary
    if ( defined $serapeum ) {
	$realign_bamFile->addCmd("lftp -e \"rm /$serapeum_root_dir/$USER/$serapeum_outpath/$targetFile; bye\" sftp://${serapeum}.qib.pbtech") if defined $targetFile;
    } else {
	if ( $localdatadir ne $execdatadir ) {
	    $realign_bamFile->addCmd("rm -f $localdatadir/$targetFile &") if defined $targetFile;
	}
    }

    if ( defined $after ) {
	$realign_bamFile->addCmd("cd $localdatadir");
	$realign_bamFile->addCmd("PBS_genRecalTable.pl \\");
	$realign_bamFile->addCmd("\t--localdatadir $localdatadir \\");
	$realign_bamFile->addCmd("\t--serapeum $serapeum \\") if defined $serapeum;
	$realign_bamFile->addCmd("\t--serapeum_path $serapeum_outpath \\") if defined $serapeum_outpath;
	$realign_bamFile->addCmd("\t--bamFile $outFile \\");
	$realign_bamFile->addCmd("\t--bamidxFile $outIndex \\");
	$realign_bamFile->addCmd("\t--bwaidx $bwaidx_orig \\");
	$realign_bamFile->addCmd("\t--gatk_resource_dir $gatk_resource_dir \\");
	$realign_bamFile->addCmd("\t--gatk_resource_ver $gatk_resource_ver \\");
	$realign_bamFile->addCmd("\t--numcpus $numcpus \\");
	$realign_bamFile->addCmd("\t--cpuMem $cpuMem \\");
	$realign_bamFile->addCmd("\t--prefix $prefix \\");
	$realign_bamFile->addCmd("\t--runtype $runtype --submit $submit --after $after");
    }
    

    switch( $submit ) {
	case 0 { $realign_bamFile->print; }
	case 1 { my $job = $realign_bamFile->submit; print "JOBID=$job\n";}
	case 2 { $realign_bamFile->_writeScript(); }
	else { $realign_bamFile->print; }
    }
}

