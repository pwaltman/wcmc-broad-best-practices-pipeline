#!/bin/env perl
use lib "/home/waltmape/elementoLabSoftware/PERL_MODULES";
#use lib "/zenodotus/dat02/elemento_lab_scratch/oelab_scratch_scratch007/kwe2001/PERL_MODULES";
use strict;
use Sets;
use PBS;
use Getopt::Long;
use Switch;
use File::Basename;
use Data::Dumper;
use POSIX qw/ceil/;
use List::Util qw[min max];

if (@ARGV == 0) {
  print "Args: --bamFile=FILE --bamidxFile=FILE --bwaidx=FILE --submit=INT\n";
  exit;
}

my $submit = 2;
my $localdatadir = "$ENV{PWD}";
my $USER="$ENV{USER}";
my $projectRootDir="FAHNSC";
my $serapeum=undef;
my $serapeum_path=undef;
# this variable allows users to manually specify the $serapeum_path, while the script will determine where to store the result based on the $localdatadir var
my $serapeum_root_dir="oelab_scratch008";
my $serapeum_outpath=undef;  
my $bamFile = undef;
my $bamidxFile = undef;
my $numcpus = 16;
my $cpuMem  = 3;
my $walltime = "24:00:00";
my $after    = undef;
my $target   = undef;
my $debug = 0;
my $gatk = "/home/waltmape/bin/GenomeAnalysisTK.jar";
my $prefix = "test_devel";
my $runtype = "BWAMEM_GATK";
my $gatk_resource_dir="$ENV{HOME}/FAHNSC/resources/grch37/gatk";
my $gatk_resource_ver="b37";
## now set vars for the necessary resource files
my $onekg_indels="$gatk_resource_dir/$gatk_resource_ver/1000G_phase1.indels.${gatk_resource_ver}.vcf";
my $mills_indels="$gatk_resource_dir/$gatk_resource_ver/Mills_and_1000G_gold_standard.indels.${gatk_resource_ver}.vcf";
my $bwaidx = "$ENV{HOME}/FAHNSC/resources/grch37/indices/bwa/broad/Homo_sapiens_assembly19.fasta";
my $bwaidx_orig=$bwaidx;  
my $bwapath = dirname( $bwaidx );

GetOptions("submit=s"            => \$submit,
	   "localdatadir=s"      => \$localdatadir,
	   "projectRootDir=s"    => \$projectRootDir,
	   "serapeum=s"          => \$serapeum,
	   "serapeum_path=s"     => \$serapeum_path,
           "bamFile=s"           => \$bamFile,
           "bamidxFile=s"        => \$bamidxFile,
	   "numcpus=s"           => \$numcpus,
	   "cpuMem=s"            => \$cpuMem,
           "walltime=s"          => \$walltime,
           "after=s"             => \$after,
	   "debug=s"             => \$debug,
	   "target=s"            => \$target,
	   "gatk=s"              => \$gatk,
           "prefix=s"            => \$prefix,
           "runtype=s"           => \$runtype,  
	   "gatk_resource_dir=s" => \$gatk_resource_dir,
	   "gatk_resource_ver=s" => \$gatk_resource_ver,
	   "bwaidx=s"            => \$bwaidx);

die( "No bam file provided for parameter --bamFile" ) if not defined $bamFile;
die( "No bam index file provided for parameter --bamidxFile" ) if not defined $bamidxFile;
die( "per process memory must be specified in terms of G (with or without the 'G'), NOT megabytes") if $cpuMem =~ /[mbMB]/ ; ## check if cpuMem is specified in terms megabytes
if ( $cpuMem =~ /gG/ ) {
    $cpuMem =~ s/gG//;
}
my $totalMem = ($numcpus * $cpuMem) + 6;
die( "per total memory of job (# cpus * memory_per_cpu) must be less than 256G; current total: $totalMem") if $totalMem > 256;

my $jobMem = ceil($totalMem / $numcpus);
if (! -e $gatk    ) {
  die("GATK jar file: $gatk does not exist.\n");
}


my $local = 0;
if ( $submit == 3 ) {
  $local = 1;
}


sub get_serapeum_path {
    my ($lclDataDir, $projRootDir) = @_;
    my @subcomps= split( /$projRootDir/, $lclDataDir );
    my $retval = "$projRootDir/$subcomps[1]";
    ## drop any double slashes - just in case
    $retval =~ s/\/\//\//g;
    return $retval; 
}


sub lftp_filechk {
    my ($user, $fname, $serapeumDir, $serapeumServ ) = @_;
    #print "echo \"ls $serapeumDir/$fname\" | sftp -b - ${user}\@${serapeumServ}.qib.pbtech 2>\&1 > /dev/null | grep -i \"not found\" | wc -l" . "\n";
    chomp( my $filechk=`echo \"ls $serapeumDir/$fname\" | sftp -b - ${user}\@${serapeumServ}.qib.pbtech 2>\&1 > /dev/null | grep -i \"not found\" | wc -l`) ;
    if ( $filechk ) {
	return 0;
    } else {
	return 1;
    }
}

sub get_serapeum_serv {
    my ($serapeumDir, $fname) = @_;
    my $retval=undef;
    my @servers = ( "serapeum1", "serapeum2" );

    foreach my $serv (@servers) {
	if ( &lftp_filechk( $USER,
			    $fname,
			    $serapeumDir,
			    $serv ) ) {
	    $retval = $serv;
	}
    }
    return $retval; 
}

if (! -e $bamFile) {
    $serapeum_outpath=&get_serapeum_path( $localdatadir, $projectRootDir ); # if ! defined $serapeum_path;
    $serapeum_path=&get_serapeum_path( $localdatadir, $projectRootDir ) if ! defined $serapeum_path;
    $serapeum=&get_serapeum_serv( $serapeum_path, $bamFile ) if ! defined $serapeum;
    if ( defined $serapeum ) {
	if ( $serapeum eq "serapeum1" ) {
	    $serapeum_root_dir="oelab_scratch008";
	} else {
	    $serapeum_root_dir="melnick-elemento_scratch001";
	}
    } else {
	die("$bamFile does not exist.\n");
    }
}

if (! -e $bamidxFile) {
    if ( defined $serapeum ) {
	## assume that everything was set while checking the location of the $bamFile AND that $bamidsFile is in the same dir
	## so, just check the location of the bamidxFile, using the vals set above
	if ( ! &lftp_filechk( $USER,
			      $bamidxFile,
			      $serapeum_path,
			      $serapeum ) ) {
	    die("$bamidxFile does not exist.\n");
	}
    }
}



## check to see if the $bamFile already exists
my $outFound=0;
my $outFile="${prefix}_realignment_targets.list";
if ( defined $serapeum ) {
    $outFound = &lftp_filechk( $USER, 
			       $outFile, 
			       $serapeum_outpath,
			       $serapeum);
} else {
    if ( -e "$localdatadir/$outFile" ) {
	$outFound = 1;
    }
}


if ( $outFound ) {
    ## looks like the output file exists, in which case, if this isn't a speedseq alignment, just start the next step
    #if ( defined $serapeum ) {
	#print "Found sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath/$outFile";
    #} else {
	#print "Found $localdatadir/$outFile";
    #}
    #print " - skipping to next step\n";

    chdir( $localdatadir );
    my $cmd = "PBS_realignBamFile.pl \\\n" .
	"\t--localdatadir $localdatadir \\\n" .
	"\t--bamFile $bamFile \\\n" .
	"\t--bamidxFile $bamidxFile \\\n" .
	"\t--targetFile $outFile \\\n" .
	"\t--bwaidx $bwaidx_orig \\\n" .
	"\t--gatk_resource_dir $gatk_resource_dir \\\n" .
	"\t--gatk_resource_ver $gatk_resource_ver \\\n" .
	"\t--numcpus $numcpus \\\n" .
	"\t--cpuMem $cpuMem \\\n" .
	"\t--prefix $prefix \\\n" .
	"\t--runtype $runtype --submit $submit --after $after";
    $cmd .= " \\\n\t--serapeum $serapeum" if defined $serapeum;
    $cmd .= " \\\n\t--serapeum_path $serapeum_outpath" if defined $serapeum_outpath;
    
    `$cmd`;
    #`$cmd | sed -e \"s:zenodotus=TRUE:zenodotus=TRUE\\nTMPDIR=\$PWD:\"> realign_bamfile.sh`;
    #`$cmd | sed -e \"s:zenodotus=TRUE:zenodotus=TRUE\\nTMPDIR=$ENV{PWD}:\"> realign_bamfile.sh`;
    #`$cmd | sed -e \"s:zenodotus=TRUE:zenodotus=TRUE\\nTMPDIR=/scratchLocal/waltmape/lftp_testing/:\"> realign_bamfile.sh`;
    chdir( "$ENV{PWD}\n" );
} else {

    my $gen_realn_targets = PBS->new;
    my $script = "${prefix}_${runtype}_gen_realn_targets_run.sh";

    if ($local == 0 ){
	$gen_realn_targets->setPlatform("panda");
	$gen_realn_targets->setEmail("pwaltman\@rockefeller.edu");
	$gen_realn_targets->setWallTime( $walltime );
	$gen_realn_targets->setNumCPUs( $numcpus );
	$gen_realn_targets->setMemory( "${cpuMem}G" );
	$gen_realn_targets->setScriptName($script);
	$gen_realn_targets->setName("${prefix}_${runtype}_gen_realn_targets");
	$gen_realn_targets->setTarget( "zenodotus" );
	
	if (defined($target)) {
	    $gen_realn_targets->setTarget($target);
	}
    }



    # Setting work directories
    my $execdatadir  = "\$TMPDIR";
    my $oeLabSharedDir = "/zenodotus/dat02/elemento_lab_scratch/oelab_scratch_scratch007/kwe2001";
    my $homedir = "$ENV{HOME}";
    my $localperlscriptsdir = "$oeLabSharedDir/programs/SNPseeqer";
    my $tmpdir = "$execdatadir/tmp";
    
    

    # If the job is run locally
    if ($local == 1) {
	$localdatadir = "$ENV{PWD}"; 
	$execdatadir  = "$ENV{PWD}";
	# Actual start to the bash script
	$gen_realn_targets->addCmd("cd $ENV{PWD}");
    } else {
	$gen_realn_targets->addCmd("cd $execdatadir");
	$gen_realn_targets->addCmd("echo \"localdatadir: $localdatadir\"");
	$gen_realn_targets->addCmd("echo \"execdatadir: $execdatadir\"");
	$gen_realn_targets->addCmd("echo \"node: \$\(uname -n\)\"");
    }



    #  copy it to remote 
    if ($local == 0) {
	if ( defined $serapeum ) {
	    $gen_realn_targets->addCmd("curdir=\$(pwd -P)");
	    $gen_realn_targets->addCmd("cd $execdatadir");
	    $gen_realn_targets->addCmd("lftp -e \"get $serapeum_path/$bamFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $gen_realn_targets->addCmd("lftp -e \"get $serapeum_path/$bamidxFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $gen_realn_targets->addCmd("cd \$curdir");
	} else {
	    $gen_realn_targets->addCmd("rsync -rptgoLv $localdatadir/$bamFile $execdatadir");
	    $gen_realn_targets->addCmd("rsync -rptgoLv $localdatadir/$bamidxFile $execdatadir");
	}
	$gen_realn_targets->addCmd("rsync -rptgoLv $gatk_resource_dir/$gatk_resource_ver/ $execdatadir/gatk");
	$gen_realn_targets->addCmd("rsync -rptgoLv $bwapath $execdatadir");
	$bwapath = basename( $bwapath );
	$bwaidx = basename( $bwaidx );
	$bwaidx = "$execdatadir/$bwapath/$bwaidx";

	$onekg_indels="$execdatadir/gatk/1000G_phase1.indels.${gatk_resource_ver}.vcf";
	$mills_indels="$execdatadir/gatk/Mills_and_1000G_gold_standard.indels.${gatk_resource_ver}.vcf";
    }
    
    
    
    # generate realignment targets
    $gen_realn_targets->addCmd("if [ -d $tmpdir ]; then");
    $gen_realn_targets->addCmd("\t mkdir $tmpdir -p");
    $gen_realn_targets->addCmd("fi");
    
    $gen_realn_targets->addCmd("echo \"generate realignment targets\"" );
    
    my $RATC_MEM = max( ($numcpus + 8), ($numcpus * $cpuMem)-8);
    my $cmd = "java -Djava.io.tmpdir=$tmpdir \\\n";
    $cmd .= "    -Xmx${RATC_MEM}g \\\n";
    $cmd .= "    -jar $gatk -T RealignerTargetCreator \\\n";
    $cmd .= "    -nt $numcpus \\\n";
    $cmd .= "    -R $bwaidx \\\n";
    $cmd .= "    -I $bamFile \\\n";
    $cmd .= "    -known $onekg_indels \\\n";
    $cmd .= "    -known $mills_indels \\\n";
    $cmd .= "    -o $outFile";
    $gen_realn_targets->addCmd( $cmd );
    
    
    $gen_realn_targets->addCmd("if [ ! -f $outFile ]; then" );
    $gen_realn_targets->addCmd("    echo \"$outFile not generated, see log for explanation\" ");
    $gen_realn_targets->addCmd("    exit 1" );
    $gen_realn_targets->addCmd("fi");
    
    if ($local == 0) { # get BAM and bam index files back, 
	if ( defined $serapeum ) {
	    ## make sure to check if the outpath exists
	    if ( ! &lftp_filechk( $USER,
				  "",
				  $serapeum_outpath,
				  $serapeum ) ) {
		$gen_realn_targets->addCmd("lftp -e \"mkdir -p /$serapeum_root_dir/$USER/$serapeum_outpath; bye\" sftp://${serapeum}.qib.pbtech"); 
	    }
	    $gen_realn_targets->addCmd("lftp -e \"put $outFile; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
	    
	}else {
	    $gen_realn_targets->addCmd("rsync -av $outFile $localdatadir");
	}
    }
    

    
    # cleanup to do if executing distantly
    $gen_realn_targets->addCmd("rm -rf $execdatadir/$bwapath &");                      # erase genome
    $gen_realn_targets->addCmd("rm -f $execdatadir/$bamFile &") if defined $bamFile;   # erase data file 1
    $gen_realn_targets->addCmd("rm -f $execdatadir/$bamidxFile &") if defined $bamidxFile;   # erase data file 1
    $gen_realn_targets->addCmd("rm -f $execdatadir/$outFile &") if defined $bamidxFile;   # erase data file 1


    if ( defined $after ) {
	$gen_realn_targets->addCmd("cd $localdatadir");
	$gen_realn_targets->addCmd("PBS_realignBamFile.pl \\");
	$gen_realn_targets->addCmd("\t--localdatadir $localdatadir \\");
	$gen_realn_targets->addCmd("\t--serapeum $serapeum \\") if defined $serapeum;
	$gen_realn_targets->addCmd("\t--serapeum_path $serapeum_outpath \\") if defined $serapeum_outpath;
	$gen_realn_targets->addCmd("\t--bamFile $bamFile \\");
	$gen_realn_targets->addCmd("\t--bamidxFile $bamidxFile \\");
	$gen_realn_targets->addCmd("\t--targetFile $outFile \\");
	$gen_realn_targets->addCmd("\t--bwaidx $bwaidx_orig \\");
	$gen_realn_targets->addCmd("\t--gatk_resource_dir $gatk_resource_dir \\");
	$gen_realn_targets->addCmd("\t--gatk_resource_ver $gatk_resource_ver \\");
	$gen_realn_targets->addCmd("\t--numcpus $numcpus \\");
	$gen_realn_targets->addCmd("\t--cpuMem $cpuMem \\");
	$gen_realn_targets->addCmd("\t--prefix $prefix \\");
	$gen_realn_targets->addCmd("\t--runtype $runtype --submit $submit --after $after");
    }
    

    switch( $submit ) {
	case 0 { $gen_realn_targets->print; }
	case 1 { my $job = $gen_realn_targets->submit; print "JOBID=$job\n";}
	case 2 { $gen_realn_targets->_writeScript(); }
	else { $gen_realn_targets->print; }
    }
}
