#README
Scripts for running the Broad Institutes Best Practices Guidlines for both DNA- and RNA-seq
data. There is one critical flaw with these scripts, which is that they are written with
the assumption that all samples were processed in a single lane. To generalize this, one would
have to modify the PBS_quickalign.pl script to generate an array job that would spawn multiple
jobs, with the final array job starting a new script that would start a to-be-written script
that would call sambamba merge.