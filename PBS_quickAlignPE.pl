#!/bin/env perl
use lib "/home/waltmape/elementoLabSoftware/PERL_MODULES";
#use lib "/zenodotus/dat02/elemento_lab_scratch/oelab_scratch_scratch007/kwe2001/PERL_MODULES";
use strict;
use Sets;
use PBS;
use Getopt::Long;
use Switch;
use File::Basename;
use Data::Dumper;

if (@ARGV == 0) {
  print "Args: --bwaidx=FILE --fastq1=FILE --fastq2=FILE --submit=INT\n";
  exit;
}

my $USER="$ENV{USER}";
my $alignAlg="mem";
my $bwaidx = "$ENV{HOME}/FAHNSC/resources/grch37/indices/bwa/broad/Homo_sapiens_assembly19.fasta";
my $bwaidx_orig=$bwaidx;
my $bwapath = dirname( $bwaidx );
my $gatk_resource_dir="$ENV{HOME}/FAHNSC/resources/grch37/gatk/";
my $gatk_resource_ver="b37";
my $genome   = undef; #"hg19";
my $target   = undef;
my $submit = 0;
my $localdatadir = "$ENV{PWD}";
#my $localdatadir = undef;
my $fastq1 = undef;
my $fastq2 = undef;
my $projectRootDir="FAHNSC";
#my $localdatadir = "$ENV{HOME}/FAHNSC/data/benchmark/synthetic/rsvsim/ref_normal/150_450_090/10/male_somatic01";
my $serapeum=undef;
my $serapeum_path=undef;
# this variable allows users to manually specify the $serapeum_path, while the script will determine where to store the result based on the $localdatadir var
my $serapeum_root_dir="oelab_scratch008";
my $serapeum_outpath=undef;  
#my $fastq1 = "male_somatic01_1.fq.gz";
#my $fastq2 = "male_somatic01_2.fq.gz";
my $sam2sbam = 1;
my $numcpus = 32;
my $cpuMem  = 4;
my $walltime = "72:00:00";
my $getsam   = 1;
my $dosplit  = 0;
my $after    = undef;
my $debug = 0;
my $cmd = "echo hello";
my $prefix = "test_devel";
my $runtype = "BWAMEM_GATK";
my $platform="Illumina";
my $sampleName="testSample";
my $libraryName="testLibrary";
my $genSamFileToo == 0;

GetOptions("alignAlg=s"          => \$alignAlg,
	   "bwaidx=s"            => \$bwaidx,
	   "gatk_resource_dir=s" => \$gatk_resource_dir,
	   "gatk_resource_ver=s" => \$gatk_resource_ver,
	   "genome=s"            => \$genome,
	   "target=s"            => \$target,
	   "submit=s"            => \$submit,
	   "localdatadir=s"      => \$localdatadir,
	   "projectRootDir=s"    => \$projectRootDir,
	   "serapeum=s"          => \$serapeum,
	   "serapeum_path=s"     => \$serapeum_path,
           "fastq1=s"            => \$fastq1,
	   "fastq2=s"            => \$fastq2,
	   "sam2sbam=s"          => \$sam2sbam,
           "numcpus=s"           => \$numcpus,
	   "cpuMem=s"            => \$cpuMem,
           "walltime=s"          => \$walltime,
	   "getsam=s"            => \$getsam,
	   "dosplit=s"           => \$dosplit,
	   "after=s"             => \$after,
           "debug=s"             => \$debug,
           "prefix=s"            => \$prefix,
	   "runtype=s"           => \$runtype,
	   "platform=s"          => \$platform,
	   "sampleName=s"        => \$sampleName, 
	   "libraryName=s"       => \$libraryName, 
	   "genSamFileToo"       => \$genSamFileToo);

die( "No fastq file provided for parameter --fastq1" ) if not defined $fastq1;
die( "No fastq file provided for parameter --fastq2" ) if not defined $fastq2;
die( "per process memory must be specified in terms of G (with or without the 'G'), NOT megabytes") if $cpuMem =~ /[mbMB]/ ; ## check if cpuMem is specified in terms megabytes
if ( $cpuMem =~ /gG/ ) {
    $cpuMem =~ s/gG//;
}
my $totalMem = $numcpus * $cpuMem;
die( "per total memory of job (# cpus * memory_per_cpu) must be less than 256G; current total: $totalMem") if $totalMem > 256;

if (defined($genome) && ($genome eq "hg19")) {
  $bwaidx =  "/pbtech_mounts/oelab_scratch002/ole2001/DATA/GENOMES/hg19/hg19bwaidx"; 
}

##  by default, speedseq will handle everything; and wont generate a sam file
if ( $alignAlg eq "speedseq" ) {
    $genSamFileToo=0;
}

my $local = 0;
if ( $submit == 3 ) {
    $local = 1;
}


sub get_serapeum_path {
    my ($lclDataDir, $projRootDir) = @_;
    my @subcomps= split( /$projRootDir/, $lclDataDir );
    my $retval = "$projRootDir/$subcomps[1]";
    ## drop any double slashes - just in case
    $retval =~ s/\/\//\//g;
    return $retval; 
}


## simple function to check if file exists on one of the serapeum servers
sub lftp_filechk {
    my ($user, $fname, $serapeumDir, $serapeumServ ) = @_;
    chomp( my $filechk=`echo \"ls $serapeumDir/$fname\" | sftp -b - ${user}\@${serapeumServ}.qib.pbtech 2>\&1 > /dev/null | grep -i \"not found\" | wc -l`) ;
    if ( $filechk ) {
	return 0;
    } else {
	return 1;
    }
}

sub get_serapeum_serv {
    my ($serapeumDir, $fname) = @_;
    my $retval=undef;
    my @servers = ( "serapeum1", "serapeum2" );

    foreach my $serv (@servers) {
	if ( &lftp_filechk( $USER,
			    $fname,
			    $serapeumDir,
			    $serv ) ) {
	    $retval = $serv;
	}
    }
    return $retval; 
}



if (! -e $fastq1) {
    ## ok, so the file isn't on zeno, as we'd hope, so check on serapeum
    ## the script will set the output path on serapeum based on the $localdata dir, but we allow uses to specify
    ## the $serapeum_path as a parameter - so that can they tell the script where to find the fasta files, but save 
    ## resulting bams in a different location
    $serapeum_outpath=&get_serapeum_path( $localdatadir, $projectRootDir ); # if ! defined $serapeum_path;
    $serapeum_path=&get_serapeum_path( $localdatadir, $projectRootDir ) if ! defined $serapeum_path;
    $serapeum=&get_serapeum_serv( $serapeum_path, $fastq1 ) if ! defined $serapeum;
    if ( defined $serapeum ) {
	if ( $serapeum eq "serapeum1" ) {
	    $serapeum_root_dir="oelab_scratch008";
	} else {
	    $serapeum_root_dir="melnick-elemento_scratch001";
	}
    } else {
	die("pbs_quickalign.pl: $fastq1 does not exist.\n") if ! defined $serapeum;
    }
}



## check to see if the $bamFile already exists
my $bamFound = 0;
my $bamidxFound = 0;
my $sFile="dedup_sorted_$prefix.bam";
my $sIdxFile="dedup_sorted_$prefix.bam.bai";
if ( $alignAlg eq "speedseq" ) {
    $sFile="${prefix}.bam";
    $sIdxFile="${prefix}.bam.bai";
}

if ( defined $serapeum ) {
    $bamFound = &lftp_filechk( $USER, 
			       $sFile, 
			       $serapeum_outpath,
			       $serapeum);
    $bamidxFound = &lftp_filechk( $USER, 
				  $sIdxFile, 
				  $serapeum_outpath,
				  $serapeum);
} else {
    if ( -e "$localdatadir/$sFile" ) {
	$bamFound = 1;
    }
    if ( -e "$localdatadir/$sIdxFile" ) {
	$bamidxFound = 1;
    }
}


if ( $bamFound && $bamidxFound ) {
    ## looks like the output file exists, in which case, if this isn't a speedseq alignment, just start the next step
    if ( $alignAlg ne "speedseq" ) {
	chdir( $localdatadir );

	my $cmd = "PBS_genRealnTargets.pl \\\n" .
	    "\t--localdatadir $localdatadir \\\n" .
	    "\t--bamFile $sFile \\\n" .
	    "\t--bamidxFile $sIdxFile \\\n" .
	    "\t--bwaidx $bwaidx_orig \\\n" .
	    "\t--gatk_resource_dir $gatk_resource_dir \\\n" .
	    "\t--gatk_resource_ver $gatk_resource_ver \\\n" .
	    "\t--numcpus $numcpus \\\n" .
	    "\t--cpuMem $cpuMem \\\n" .
	    "\t--prefix $prefix \\\n" .
	    "\t--runtype $runtype --submit $submit --after $after";
	$cmd .= " \\\n\t--serapeum $serapeum" if defined $serapeum;
	$cmd .= " \\\n\t--serapeum_path $serapeum_outpath" if defined $serapeum_outpath;
	#print "to execute:#####################################\n";
	#print "$cmd\n";
	#print "################################################\n";
	`$cmd`;
	#`$cmd | sed -e \"s:zenodotus=TRUE:zenodotus=TRUE\\nTMPDIR=\$PWD:\"> gen_realn_targets.sh`;
	#`$cmd | sed -e \"s:zenodotus=TRUE:zenodotus=TRUE\\nTMPDIR=$ENV{PWD}:\"> gen_realn_targets.sh`;
	#`$cmd | sed -e \"s:zenodotus=TRUE:zenodotus=TRUE\\nTMPDIR=/scratchLocal/waltmape/lftp_testing/:\"> gen_realn_targets.sh`;
	chdir( "$ENV{PWD}\n" );
    }
} else {
    ## looks like the output files haven't been generated, so generate a qsub file as necessary
    my $align_and_prep = PBS->new;
    #my $script = "$fastq1.$fastq2.${alignAlg}job";
    my $script = "${sampleName}_${alignAlg}_align_and_prep.sh";



    if ($local == 0 ){
	$align_and_prep->setPlatform("panda");
	$align_and_prep->setEmail("pwaltman\@rockefeller.edu");
	$align_and_prep->setWallTime( $walltime );
	$align_and_prep->setNumCPUs( $numcpus );
	$align_and_prep->setMemory( "${cpuMem}G" );
	$align_and_prep->setScriptName($script);
	$align_and_prep->setName("${sampleName}_${alignAlg}_align_and_prep");
	$align_and_prep->setTarget( "zenodotus" );
	
	if (defined($target)) {
	    $align_and_prep->setTarget($target);
	}
    }


    # Setting work directories
    my $execdatadir  = "\$TMPDIR";
    my $oeLabSharedDir = "/zenodotus/dat02/elemento_lab_scratch/oelab_scratch_scratch007/kwe2001";
    my $homedir = "$ENV{HOME}";
    my $localbwadir = "\$\{BINDIR\}/speedseq/bin";
    my $localperlscriptsdir = "$oeLabSharedDir/programs/SNPseeqer";
    
    my $RGID = "\"\@RG\\tID:test1\\tSM:$sampleName\\tLB:$libraryName\\tPL:$platform\\t\" ";
    my $errorlog = "bwa_$alignAlg.log.txt";
    my $tmpdir = "$execdatadir/tmp";
    my $SortedDedupedIndexBam = "$execdatadir/dedup_sorted_$prefix.bam";
    my $bamFile = "dedup_sorted_$prefix.bam";
    my $bamidxFile = "dedup_sorted_$prefix.bam.bai";


    # If the job is run locally
    if ($local == 1) {
	$localdatadir = "$ENV{PWD}"; 
	$execdatadir  = "$ENV{PWD}";
	# Actual start to the bash script
	$align_and_prep->addCmd("cd $ENV{PWD}");
    } else {
	$align_and_prep->addCmd("cd $execdatadir");
	$align_and_prep->addCmd("echo \"localdatadir: $localdatadir\"");
	$align_and_prep->addCmd("echo \"execdatadir: $execdatadir\"");
	$align_and_prep->addCmd("echo \"node: \$\(uname -n\)\"");
    }


   #  copy it to remote 
    if ($local == 0) {
	if ( defined $serapeum ) {
	    $align_and_prep->addCmd("curdir=\$(pwd -P)");
	    $align_and_prep->addCmd("cd $execdatadir");
	    $align_and_prep->addCmd("lftp -e \"get $serapeum_path/$fastq1; bye\" sftp://${serapeum}.qib.pbtech");
	    $align_and_prep->addCmd("lftp -e \"get $serapeum_path/$fastq2; bye\" sftp://${serapeum}.qib.pbtech");
	    $align_and_prep->addCmd("cd \$curdir");
	} else {
	    $align_and_prep->addCmd("rsync -rptgoLv $localdatadir/$fastq1 $execdatadir");
	    $align_and_prep->addCmd("rsync -rptgoLv $localdatadir/$fastq2 $execdatadir");
	}
	$bwapath = dirname( $bwaidx );
	$align_and_prep->addCmd("rsync -rptgoLv $bwapath $execdatadir");
	$bwapath = basename( $bwapath );
	$bwaidx = basename( $bwaidx );
	$bwaidx = "$execdatadir/$bwapath/$bwaidx";
    }
    


    sub debugStmtWrapper {
	my $CMD = shift;
	die "Must specify the command to add to the script" if not defined $CMD;
	my $LOG = shift // $errorlog;
	my $ALIGN_AND_PREP = shift // $align_and_prep;
	my $DBG = shift // $debug;
	
	if($DBG==0){
	    $ALIGN_AND_PREP->addCmd("$CMD");
	}
	else{
	    $ALIGN_AND_PREP->addCmd("$CMD >> $LOG");
	    $ALIGN_AND_PREP->addCmd('if $! -ne 0; then');
	    $ALIGN_AND_PREP->addCmd("Error in command: $CMD" );
	    $ALIGN_AND_PREP->addCmd("exit 1");
	    $ALIGN_AND_PREP->addCmd("else");
	    $ALIGN_AND_PREP->addCmd("rm $LOG");
	    $ALIGN_AND_PREP->addCmd("fi");
	}
    }
    
    
    sub alnAlignFastq {
	my $fastq = shift;
	my $alnOut = "$fastq.bwa";
	die( "User must specify fastq file to alnAlignFastq fn\n" ) if not defined $fastq;
	my $dbg = shift // 1; ##  default to 1 if debug is not set
	
	my $cmd = "$localbwadir/bwa aln -t $numcpus $bwaidx $execdatadir/$fastq > $execdatadir/$alnOut"; 
	&debugStmtWrapper( $cmd,
			   $errorlog,
			   $align_and_prep,
			   $debug );
	return $alnOut;
    }
    


    my $alnOut1 = undef;
    my $alnOut2 = undef;

    if ( $alignAlg eq "aln" ) {
	# Step 1: Performing alignments using bwa
	$alnOut1 = &alnAlignFastq( $fastq1, $debug );
	$alnOut2 = &alnAlignFastq( $fastq2, $debug );
	
	#$align_and_prep->addCmd("wait");
    }

    switch( $alignAlg) {
	case "aln" { $cmd = "$localbwadir/bwa sampe -r $RGID \\\n\t $bwaidx \\\n\t $execdatadir/$alnOut1 $execdatadir/$alnOut2 \\\n\t $execdatadir/$fastq1 $execdatadir/$fastq2"; }
	case "mem" { $cmd = "$localbwadir/bwa mem -M -R $RGID -t $numcpus \\\n\t $bwaidx \\\n\t $execdatadir/$fastq1 $execdatadir/$fastq2"; }
	case "speedseq" { $cmd="speedseq align -t $numcpus -o $prefix \\\n\t -R $RGID \\\n\t $bwaidx \\\n\t $execdatadir/$fastq1 \\\n\t $execdatadir/$fastq2"; }
	else { die( "invalid bwa algorithm specified: $alignAlg\n" ); }
    }
    
    if ( $genSamFileToo == 0 ) {
	if ($alignAlg ne "speedseq" ) {
	    $cmd = $cmd . " \\\n\t" . 
		"| samblaster -M --addMateTags \\\n\t" .
		"| samtools view -b -S -u - \\\n\t" .
		"| sambamba sort -t $numcpus -m ${cpuMem}G --tmpdir \"$tmpdir\" -o $SortedDedupedIndexBam /dev/stdin";
	    
	}
	&debugStmtWrapper( $cmd,
			   $errorlog,
			   $align_and_prep,
			   $debug );
    } else {
	my $bwaOut = " > $execdatadir/$prefix.sam";
	$cmd .= $bwaOut;
	&debugStmtWrapper( $cmd,
			   $errorlog,
			   $align_and_prep,
			   $debug );
	## add command to sort and index
	$cmd = "samblaster -M --addMateTags -i $bwaOut \\\n\t" .
	    "| samtools view -b -S -u - \\\n\t" .
	    "| sambamba sort -t $numcpus -m $cpuMem --tmpdir \"$tmpdir\" -o $SortedDedupedIndexBam /dev/stdin";
	&debugStmtWrapper( $cmd,
			   $errorlog,
			   $align_and_prep,
			   $debug );
    }
    
    if ($local == 0) { # get BAM and bam index files back, 
	
	if ( defined $serapeum ) {
	    ## make sure to check if the outpath exists
	    if ( ! &lftp_filechk( $USER,
				  "",
				  $serapeum_outpath,
				  $serapeum ) ) {
		$align_and_prep->addCmd("lftp -e \"mkdir -p /$serapeum_root_dir/$USER/$serapeum_outpath; bye\" sftp://${serapeum}.qib.pbtech"); 
	    }
	    
	    if ( $alignAlg eq "speedseq" ) {
		
		$align_and_prep->addCmd("lftp -e \"put $execdatadir/${prefix}.bam; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
		$align_and_prep->addCmd("lftp -e \"put $execdatadir/${prefix}.bam.bai; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
		$align_and_prep->addCmd("lftp -e \"put $execdatadir/${prefix}.discordants.bam; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
		$align_and_prep->addCmd("lftp -e \"put $execdatadir/${prefix}.discordants.bam.bai; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
		$align_and_prep->addCmd("lftp -e \"put $execdatadir/${prefix}.splitters.bam; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
		$align_and_prep->addCmd("lftp -e \"put $execdatadir/${prefix}.splitters.bam.bai; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
	    } else {
		$align_and_prep->addCmd("lftp -e \"put $SortedDedupedIndexBam; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
		$align_and_prep->addCmd("lftp -e \"put ${SortedDedupedIndexBam}.bai; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
	    }
	} else {
	    #if ( ! &lftp_filechk( $USER,
	    #		      $fname,
	    #		      $serapeumDir,
	    #		      $serapeum ) ) {
	    #    $align_and_prep->addCmd("lftp -e \"mkdir -p /$serapeum_root_dir/$USER/$serapeum_outpath; bye\" sftp://${serapeum}.qib.pbtech"); 
	    #}
	    if ( $alignAlg eq "speedseq" ) {
		$align_and_prep->addCmd("rsync -av $execdatadir/${prefix}.bam $localdatadir");
		$align_and_prep->addCmd("rsync -av $execdatadir/${prefix}.bam.bai $localdatadir");
		$align_and_prep->addCmd("rsync -av $execdatadir/${prefix}.discordants.bam $localdatadir");
		$align_and_prep->addCmd("rsync -av $execdatadir/${prefix}.discordants.bam.bai $localdatadir");
		$align_and_prep->addCmd("rsync -av $execdatadir/${prefix}.splitters.bam $localdatadir");
		$align_and_prep->addCmd("rsync -av $execdatadir/${prefix}.splitters.bam.bai $localdatadir");
	    } else {
		$align_and_prep->addCmd("rsync -av $SortedDedupedIndexBam $localdatadir");
		$align_and_prep->addCmd("rsync -av $SortedDedupedIndexBam.bai $localdatadir");
	    }
	}
    }

    # cleanup to do if executing distantly
    #$align_and_prep->addCmd("rm -rf $execdatadir/$bwapath &");                      # erase genome
    $align_and_prep->addCmd("rm -f $execdatadir/*.bam &") if defined $bamFile;   # erase data file 1
    $align_and_prep->addCmd("rm -f $execdatadir/*.bai &") if defined $bamidxFile;   # erase data file 1


    if ( $alignAlg ne "speedseq" ) {
	if ( defined $after ) {
	    $align_and_prep->addCmd("cd $localdatadir");
	    $align_and_prep->addCmd("PBS_genRealnTargets.pl \\");
	    $align_and_prep->addCmd("\t--localdatadir $localdatadir \\");
	    $align_and_prep->addCmd("\t--serapeum $serapeum \\") if defined $serapeum;
	    $align_and_prep->addCmd("\t--serapeum_path $serapeum_outpath \\") if defined $serapeum_outpath;
	    $align_and_prep->addCmd("\t--bamFile $bamFile \\");
	    $align_and_prep->addCmd("\t--bamidxFile $bamidxFile \\");
	    $align_and_prep->addCmd("\t--bwaidx $bwaidx_orig \\");
	    $align_and_prep->addCmd("\t--gatk_resource_dir $gatk_resource_dir \\");
	    $align_and_prep->addCmd("\t--gatk_resource_ver $gatk_resource_ver \\");
	    $align_and_prep->addCmd("\t--numcpus $numcpus \\");
	    $align_and_prep->addCmd("\t--cpuMem $cpuMem \\");
	    $align_and_prep->addCmd("\t--prefix $prefix \\");
	    $align_and_prep->addCmd("\t--runtype $runtype --submit $submit --after $after");
	}
    }
    

    switch( $submit ) {
	case 0 { $align_and_prep->print; }
	case 1 { my $job = $align_and_prep->submit; print "JOBID=$job\n";}
	case 2 { $align_and_prep->_writeScript(); }
	else { $align_and_prep->print; }
    }
}


