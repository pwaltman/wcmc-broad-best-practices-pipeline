#!/bin/env perl
use lib "/home/waltmape/elementoLabSoftware/PERL_MODULES";
use strict;
use Sets;
use PBS;
use Getopt::Long;
use Switch;
use File::Basename;
use Data::Dumper;
use POSIX qw/ceil/;
use List::Util qw[min max];

if (@ARGV == 0) {
  print "Args: --bwaidx=FILE --fastq1=FILE --fastq2=FILE --submit=INT\n";
  exit;
}

my $submit = 0;
my $localdatadir = "$ENV{PWD}";
my $USER="$ENV{USER}";
my $projectRootDir="FAHNSC";
my $serapeum=undef;
my $serapeum_path=undef;
# this variable allows users to manually specify the $serapeum_path, while the script will determine where to store the result based on the $localdatadir var
my $serapeum_root_dir="oelab_scratch008";
my $serapeum_outpath=undef;  
my $bamFile = undef;
my $bamidxFile = undef;
my $bwaidx = "$ENV{HOME}/FAHNSC/resources/grch37/indices/bwa/broad/Homo_sapiens_assembly19.fasta";
my $bwapath = dirname( $bwaidx );
my $bwaidx_orig = $bwaidx;
my $gatk = "/home/waltmape/bin/GenomeAnalysisTK.jar";
my $gatk_resource_dir="$ENV{HOME}/FAHNSC/resources/grch37/gatk/";
my $gatk_resource_ver="b37";
my $genome   = undef; #"hg19";
my $target   = undef;
my $numcpus = 16;
my $cpuMem  = 5;
my $walltime = "48:00:00";
my $after    = undef;
my $rnaseq   = undef;
my $debug = 0;
my $cmd = "echo hello";
my $prefix = "test_devel";
my $runtype = "BWAMEM_GATK";
my $platform="Illumina";
my $sampleName="testSample";
my $libraryName="testLibrary";
my $genSamFileToo == 0;

GetOptions("bwaidx=s"            => \$bwaidx,
	   "gatk"                => \$gatk,
	   "gatk_resource_dir=s" => \$gatk_resource_dir,
	   "gatk_resource_ver=s" => \$gatk_resource_ver,
	   "genome=s"            => \$genome,
	   "target=s"            => \$target,
	   "submit=s"            => \$submit,
	   "localdatadir=s"      => \$localdatadir,
	   "projectRootDir=s"    => \$projectRootDir,
	   "serapeum=s"          => \$serapeum,
	   "serapeum_path=s"     => \$serapeum_path,
           "bamFile=s"           => \$bamFile,
           "bamidxFile=s"        => \$bamidxFile,
           "numcpus=s"           => \$numcpus,
	   "cpuMem=s"            => \$cpuMem,
           "walltime=s"          => \$walltime,
	   "after=s"             => \$after,
	   "rnaseq=s"            => \$rnaseq,
           "debug=s"             => \$debug,
           "prefix=s"            => \$prefix,
	   "runtype=s"           => \$runtype,
	   "platform=s"          => \$platform,
	   "sampleName=s"        => \$sampleName, 
	   "libraryName=s"       => \$libraryName, 
	   "genSamFileToo"       => \$genSamFileToo );

die( "No bam file provided for parameter --bamFile" ) if not defined $bamFile;
die( "No bam idx file provided for parameter --bamidxFile" ) if not defined $bamidxFile;
die( "per process memory must be specified in terms of G (with or without the 'G'), NOT megabytes") if $cpuMem =~ /[mbMB]/ ; ## check if cpuMem is specified in terms megabytes
if ( $cpuMem =~ /gG/ ) {
    $cpuMem =~ s/gG//;
}
my $totalMem = $numcpus * $cpuMem;
die( "per total memory of job (# cpus * memory_per_cpu) must be less than 128G; current total: $totalMem") if $totalMem > 128;

if (defined($genome) && ($genome eq "hg19")) {
  $bwaidx =  "/pbtech_mounts/oelab_scratch002/ole2001/DATA/GENOMES/hg19/hg19bwaidx"; 
}

my $jobMem = ceil($totalMem / $numcpus);
if (! -e $gatk    ) {
  die("GATK jar file: $gatk does not exist.\n");
}


my $local = 0;
if ( $submit == 3 ) {
  $local = 1;
}


sub get_serapeum_path {
    my ($lclDataDir, $projRootDir) = @_;
    my @subcomps= split( /$projRootDir/, $lclDataDir );
    my $retval = "$projRootDir/$subcomps[1]";
    ## drop any double slashes - just in case
    $retval =~ s/\/\//\//g;
    return $retval; 
}


sub lftp_filechk {
    my ($user, $fname, $serapeumDir, $serapeumServ ) = @_;
    chomp( my $filechk=`echo \"ls $serapeumDir/$fname\" | sftp -b - ${user}\@${serapeumServ}.qib.pbtech 2>\&1 > /dev/null | grep -i \"not found\" | wc -l`) ;
    if ( $filechk ) {
	return 0;
    } else {
	return 1;
    }
}

sub get_serapeum_serv {
    my ($serapeumDir, $fname) = @_;
    my $retval=undef;
    my @servers = ( "serapeum1", "serapeum2" );

    foreach my $serv (@servers) {
	if ( &lftp_filechk( $USER,
			    $fname,
			    $serapeumDir,
			    $serv ) ) {
	    $retval = $serv;
	}
    }
    return $retval; 
}

if (! -e $bamFile) {
    $serapeum_outpath=&get_serapeum_path( $localdatadir, $projectRootDir ); # if ! defined $serapeum_path;
    $serapeum_path=&get_serapeum_path( $localdatadir, $projectRootDir ) if ! defined $serapeum_path;
    $serapeum=&get_serapeum_serv( $serapeum_path, $bamFile ) if ! defined $serapeum;
    if ( defined $serapeum ) {
	if ( $serapeum eq "serapeum1" ) {
	    $serapeum_root_dir="oelab_scratch008";
	} else {
	    $serapeum_root_dir="melnick-elemento_scratch001";
	}
    } else {
	die("$bamFile does not exist.\n");
    }
}

if (! -e $bamidxFile) {
    if ( defined $serapeum ) {
	## assume that everything was set while checking the location of the $bamFile AND that $bamidsFile is in the same dir
	## so, just check the location of the bamidxFile, using the vals set above
	my $dbg = &lftp_filechk( $USER,
				 $bamidxFile,
				 $serapeum_path,
				 $serapeum );
	print "dbg: $dbg\n";
	if ( ! &lftp_filechk( $USER,
			      $bamidxFile,
			      $serapeum_path,
			      $serapeum ) ) {
	    die("$bamidxFile does not exist.\n");
	}
    }
}

my $dedup_and_sort_bam = PBS->new;
my $script = "${sampleName}_dedup_and_sort_bam.sh";
my $errorlog = "dedup.log.txt";




# Setting work directories
my $localdatadir = "$ENV{PWD}";
my $execdatadir  = "\$TMPDIR";
my $oeLabSharedDir = "/zenodotus/dat02/elemento_lab_scratch/oelab_scratch_scratch007/kwe2001";
my $homedir = "$ENV{HOME}";
my $localperlscriptsdir = "$oeLabSharedDir/programs/SNPseeqer";
my $tmpdir = "$execdatadir/tmp";

## declare the output variables
my $outFile = "dedup_sorted_$prefix.bam";
my $outIndex = "$outFile.bai";


# legacy var namess from code copied from PBS_quickAlignPE.pl
my $outFound = 0;
my $outidxFound = 0;
if ( defined $serapeum ) {
    $outFound = &lftp_filechk( $USER, 
			       $outFile, 
			       $serapeum_outpath,
			       $serapeum);
    $outidxFound = &lftp_filechk( $USER, 
				  $outIndex,
				  $serapeum_outpath,
				  $serapeum);
} else {
    if ( -e "$localdatadir/$outFile" ) {
	$outFound = 1;
    }
    if ( -e "$localdatadir/$outIndex" ) {
	$outidxFound = 1;
    }
}


if ($local == 0 ){
    $dedup_and_sort_bam->setPlatform("panda");
    $dedup_and_sort_bam->setEmail("pwaltman\@rockefeller.edu");
    $dedup_and_sort_bam->setWallTime( $walltime );
    $dedup_and_sort_bam->setNumCPUs( $numcpus );
    $dedup_and_sort_bam->setMemory( "${cpuMem}G" );
    $dedup_and_sort_bam->setScriptName($script);
    $dedup_and_sort_bam->setName("${sampleName}_dedup_and_sort_bam");
    $dedup_and_sort_bam->setTarget( "zenodotus" );

    if (defined($target)) {
	$dedup_and_sort_bam->setTarget($target);
    }
}


# If the job is run locally
if ($local == 1) {
  $localdatadir = "$ENV{PWD}"; 
  $execdatadir  = "$ENV{PWD}";
  # Actual start to the bash script
  $dedup_and_sort_bam->addCmd("cd $ENV{PWD}");
}
else {
    $dedup_and_sort_bam->addCmd("cd $execdatadir");
    $dedup_and_sort_bam->addCmd("echo \"localdatadir: $localdatadir\"");
    $dedup_and_sort_bam->addCmd("echo \"execdatadir: $execdatadir\"");
    $dedup_and_sort_bam->addCmd("echo \"node: \$\(uname -n\)\"");
}

$dedup_and_sort_bam->addCmd("mkdir -p $tmpdir");


## check if the dedup'd & sorted file exists
if ( $outFound && $outidxFound ) {
    ## if the dedup'd and sorted bam exists, check if this is an rnaseq file (user-specified)
    if ( defined $rnaseq ) {
	## if this isn't an rna-seq sample, do nothing else, i.e. if it is an rnaseq file, copy the dedup'd file so that we can run Split'N'Trim on it
	if ($local == 0) {
	    if ( defined $serapeum ) {
		## make sure to check if the outpath exists
		if ( ! &lftp_filechk( $USER,
				      "",
				      $serapeum_outpath,
				      $serapeum ) ) {
		    $dedup_and_sort_bam->addCmd("lftp -e \"mkdir -p /$serapeum_root_dir/$USER/$serapeum_outpath; bye\" sftp://${serapeum}.qib.pbtech"); 
		}
		$dedup_and_sort_bam->addCmd("lftp -e \"put $outFile; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
		$dedup_and_sort_bam->addCmd("lftp -e \"put $outIndex; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
		
 	    } else {
		$dedup_and_sort_bam->addCmd("rsync -rptgoLv $localdatadir/$outFile $execdatadir");
		$dedup_and_sort_bam->addCmd("rsync -rptgoLv $localdatadir/$outIndex $execdatadir");
	    }
	}
    }
} else {
    ## if not, copy over the bamFile & index
    $dedup_and_sort_bam->addCmd("");
    if ($local == 0) {
	$dedup_and_sort_bam->addCmd("rsync -rptgoLv $localdatadir/$bamFile $execdatadir");
	$dedup_and_sort_bam->addCmd("rsync -rptgoLv $localdatadir/$bamidxFile $execdatadir");
    }


    my $coord_sort=`samtools view -H $bamFile | grep \"SO:coordinate\" | wc -l`;
    if ( $coord_sort == 1 ) {
	$cmd="sambamba markdup -t $numcpus --overflow-list-size 600000 --hash-table-size 5000000 --tmpdir=\"$tmpdir\" $execdatadir/$bamFile $execdatadir/$outFile";
    } else {
	$cmd = "sambamba markdup -t $numcpus --tmpdir \"$tmpdir\" $execdatadir/$bamFile /dev/stdout \\\n\t" .
	    "| samtools view -b -S -u - \\\n\t" .
	    "| sambamba sort -t $numcpus -m ${cpuMem}G --tmpdir \"$tmpdir\" -o $execdatadir/$outFile /dev/stdin";
	}
    $dedup_and_sort_bam->addCmd( $cmd );
    $dedup_and_sort_bam->addCmd("if [ ! -f $execdatadir/$outFile ]; then" );
    $dedup_and_sort_bam->addCmd("    exit 1");
    $dedup_and_sort_bam->addCmd("fi");
    
    
    if ($local == 0) { # get BAM and bam index files back, 
	if ( defined $serapeum ) {
	    $dedup_and_sort_bam->addCmd("curdir=\$(pwd -P)");
	    $dedup_and_sort_bam->addCmd("cd $execdatadir");
	    $dedup_and_sort_bam->addCmd("lftp -e \"get $serapeum_path/$bamFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $dedup_and_sort_bam->addCmd("lftp -e \"get $serapeum_path/$bamidxFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $dedup_and_sort_bam->addCmd("cd \$curdir");
	} else {
	    $dedup_and_sort_bam->addCmd("rsync -av $execdatadir/$outFile $localdatadir");
	    $dedup_and_sort_bam->addCmd("rsync -av $execdatadir/$outIndex $localdatadir");
	}
    }
    $dedup_and_sort_bam->addCmd("");
}




## since this is RNA-seq data, it looks like we need to re-run the split-'n-trim process too 
##  before we  start the other GATK steps
if ( defined ($rnaseq) ) {
    my $bwaidx_orig=$bwaidx;
    my $bwapath = dirname( $bwaidx );


    my $RATC_MEM = max( ($numcpus + 8), ($numcpus * $cpuMem)-8);
    my $splitBam = "split_${outFile}";
    $cmd = "java -Djava.io.tmpdir=$tmpdir \\\n";
    $cmd .= "    -Xmx${RATC_MEM}g \\\n";
    $cmd .= "    -jar $gatk -T SplitNCigarReads \\\n";
    $cmd .= "    -R $bwaidx \\\n";
    $cmd .= "    -I $outFile \\\n";
    $cmd .= "    -o $splitBam \\\n";
    $cmd .= "    -rf ReassignOneMappingQuality \\\n";
    $cmd .= "    -RMQF 255 \\\n";
    $cmd .= "    -RMQT 60 \\\n";
    $cmd .= "    -U ALLOW_N_CIGAR_READS";
    $dedup_and_sort_bam->addCmd( $cmd );

    ## reset the $outFile var
    $outFile = $splitBam;
    $outIndex = $splitBam;
    $outIndex =~ s/\.bam$/\.bai/;


    if ($local == 0) { # get BAM and bam index files back, 
	if ( defined $serapeum ) {
	    $dedup_and_sort_bam->addCmd("curdir=\$(pwd -P)");
	    $dedup_and_sort_bam->addCmd("cd $execdatadir");
	    $dedup_and_sort_bam->addCmd("lftp -e \"get $serapeum_path/$bamFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $dedup_and_sort_bam->addCmd("lftp -e \"get $serapeum_path/$bamidxFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $dedup_and_sort_bam->addCmd("cd \$curdir");
	} else {
	    $dedup_and_sort_bam->addCmd("rsync -av $execdatadir/$outFile $localdatadir");
	    $dedup_and_sort_bam->addCmd("rsync -av $execdatadir/$outIndex $localdatadir");
	}
    }
    $dedup_and_sort_bam->addCmd("");
}


if ( defined $after ) {
    $dedup_and_sort_bam->addCmd("cd $localdatadir");
    $dedup_and_sort_bam->addCmd("PBS_genRecalTable.pl \\");
    $dedup_and_sort_bam->addCmd("\t--localdatadir $localdatadir \\");
    $dedup_and_sort_bam->addCmd("\t--serapeum $serapeum \\") if defined $serapeum;
    $dedup_and_sort_bam->addCmd("\t--serapeum_path $serapeum_outpath \\") if defined $serapeum_outpath;
    $dedup_and_sort_bam->addCmd("\t--bamFile $outFile \\");
    $dedup_and_sort_bam->addCmd("\t--bamidxFile $outIndex \\");
    $dedup_and_sort_bam->addCmd("\t--bwaidx $bwaidx_orig \\");
    $dedup_and_sort_bam->addCmd("\t--gatk_resource_dir $gatk_resource_dir \\");
    $dedup_and_sort_bam->addCmd("\t--gatk_resource_ver $gatk_resource_ver \\");
    $dedup_and_sort_bam->addCmd("\t--numcpus $numcpus \\");
    $dedup_and_sort_bam->addCmd("\t--cpuMem $cpuMem \\");
    $dedup_and_sort_bam->addCmd("\t--prefix $prefix \\");
    $dedup_and_sort_bam->addCmd("\t--runtype $runtype --submit $submit --after $after");
}


switch( $submit ) {
    case 0 { $dedup_and_sort_bam->print; }
    case 1 { my $job = $dedup_and_sort_bam->submit; print "JOBID=$job\n";}
    case 2 { $dedup_and_sort_bam->_writeScript(); }
    else { $dedup_and_sort_bam->print; }
}

