#!/bin/env perl
use lib "/home/waltmape/elementoLabSoftware/PERL_MODULES";
use strict;
use Sets;
use PBS;
use Getopt::Long;
use Switch;
use File::Basename;
use Data::Dumper;
use POSIX qw/ceil/;
use List::Util qw[min max];

if (@ARGV == 0) {
  print "Args: --bamFile=FILE --bamidxFile=FILE --bwaidx=FILE --submit=INT\n";
  exit;
}

my $submit = 2;
my $localdatadir = "$ENV{PWD}";
my $USER="$ENV{USER}";
my $projectRootDir="FAHNSC";
my $serapeum=undef;
my $serapeum_path=undef;
# this variable allows users to manually specify the $serapeum_path, while the script will determine where to store the result based on the $localdatadir var
my $serapeum_root_dir="oelab_scratch008";
my $serapeum_outpath=undef;  
my $bamFile = undef;
my $bamidxFile = undef;
my $recalTable = undef;
my $numcpus = 16;
my $cpuMem  = 3;
my $walltime = "24:00:00";
my $after    = undef;
my $rnaseq   = undef;
my $target   = undef;
my $debug = 0;
my $gatk = "/home/waltmape/bin/GenomeAnalysisTK.jar";
my $prefix = "test_devel";
my $runtype = "BWAMEM_GATK";
## now set vars for the necessary resource files
my $bwaidx = "$ENV{HOME}/FAHNSC/resources/grch37/indices/bwa/broad/Homo_sapiens_assembly19.fasta";
my $bwaidx_orig=$bwaidx;  
my $bwapath = dirname( $bwaidx );


GetOptions("submit=s"        => \$submit,
	   "localdatadir=s"  => \$localdatadir,
	   "projectRootDir=s"=> \$projectRootDir,
	   "serapeum=s"      => \$serapeum,
	   "serapeum_path=s" => \$serapeum_path,
           "bamFile=s"       => \$bamFile,
           "bamidxFile=s"    => \$bamidxFile,
           "recalTable=s"    => \$recalTable,
	   "numcpus=s"       => \$numcpus,
	   "cpuMem=s"        => \$cpuMem,
           "walltime=s"      => \$walltime,
           "after=s"         => \$after,
	   "rnaseq=s"        => \$rnaseq,
	   "debug=s"         => \$debug,
	   "target=s"        => \$target,
	   "gatk=s"          => \$gatk,
           "prefix=s"        => \$prefix,
           "runtype=s"       => \$runtype,  
	   "bwaidx=s"        => \$bwaidx);

die( "No bam file provided for parameter --bamFile" ) if not defined $bamFile;
die( "No bam index file provided for parameter --bamidxFile" ) if not defined $bamidxFile;
die( "No target file provided for parameter --recalTable" ) if not defined $recalTable;
die( "per process memory must be specified in terms of G (with or without the 'G'), NOT megabytes") if $cpuMem =~ /[mbMB]/ ; ## check if cpuMem is specified in terms megabytes
if ( $cpuMem =~ /gG/ ) {
    $cpuMem =~ s/gG//;
}
my $totalMem = ($numcpus * $cpuMem) + 6;
die( "per total memory of job (# cpus * memory_per_cpu) must be less than 256G; current total: $totalMem") if $totalMem > 256;

my $jobMem = ceil($totalMem / $numcpus);
if (! -e $gatk    ) {
  die("GATK jar file: $gatk does not exist.\n");
}


my $local = 0;
if ( $submit == 3 ) {
  $local = 1;
}


sub get_serapeum_path {
    my ($lclDataDir, $projRootDir) = @_;
    my @subcomps= split( /$projRootDir/, $lclDataDir );
    my $retval = "$projRootDir/$subcomps[1]";
    ## drop any double slashes - just in case
    $retval =~ s/\/\//\//g;
    return $retval; 
}


sub lftp_filechk {
    my ($user, $fname, $serapeumDir, $serapeumServ ) = @_;
    chomp( my $filechk=`echo \"ls $serapeumDir/$fname\" | sftp -b - ${user}\@${serapeumServ}.qib.pbtech 2>\&1 > /dev/null | grep -i \"not found\" | wc -l`) ;
    if ( $filechk ) {
	return 0;
    } else {
	return 1;
    }
}

sub get_serapeum_serv {
    my ($serapeumDir, $fname) = @_;
    my $retval=undef;
    my @servers = ( "serapeum1", "serapeum2" );

    foreach my $serv (@servers) {
	if ( &lftp_filechk( $USER,
			    $fname,
			    $serapeumDir,
			    $serv ) ) {
	    $retval = $serv;
	}
    }
    return $retval; 
}

if (! -e $bamFile) {
    $serapeum_outpath=&get_serapeum_path( $localdatadir, $projectRootDir ); # if ! defined $serapeum_path;
    $serapeum_path=&get_serapeum_path( $localdatadir, $projectRootDir ) if ! defined $serapeum_path;
    $serapeum=&get_serapeum_serv( $serapeum_path, $bamFile ) if ! defined $serapeum;
    if ( defined $serapeum ) {
	if ( $serapeum eq "serapeum1" ) {
	    $serapeum_root_dir="oelab_scratch008";
	} else {
	    $serapeum_root_dir="melnick-elemento_scratch001";
	}
    } else {
	die("$bamFile does not exist.\n");
    }
}

if (! -e $bamidxFile) {
    if ( defined $serapeum ) {
	## assume that everything was set while checking the location of the $bamFile AND that $bamidsFile is in the same dir
	## so, just check the location of the bamidxFile, using the vals set above
	if ( ! &lftp_filechk( $USER,
			      $bamidxFile,
			      $serapeum_path,
			      $serapeum ) ) {
	    die("$bamidxFile does not exist.\n");
	}
    }
}

if (! -e $recalTable) {
    if ( defined $serapeum ) {
	## assume that everything was set while checking the location of the $bamFile AND that $bamidsFile is in the same dir
	## so, just check the location of the bamidxFile, using the vals set above
	if ( ! &lftp_filechk( $USER,
			      $recalTable,
			      $serapeum_path,
			      $serapeum ) ) {
	    die("$recalTable does not exist.\n");
	}
    }
}




## declare the output variables
my $outFile="recal_${bamFile}";
my $outIndex=$outFile;
$outIndex=~ s/bam$/bai/;


# legacy var namess from code copied from PBS_quickAlignPE.pl
my $outFound = 0;
my $outidxFound = 0;
if ( defined $serapeum ) {
    $outFound = &lftp_filechk( $USER, 
			       $outFile, 
			       $serapeum_outpath,
			       $serapeum);
    $outidxFound = &lftp_filechk( $USER, 
				  $outIndex,
				  $serapeum_outpath,
				  $serapeum);
} else {
    if ( -e "$localdatadir/$outFile" ) {
	$outFound = 1;
    }
    if ( -e "$localdatadir/$outIndex" ) {
	$outidxFound = 1;
    }
}


if ( $outFound && $outidxFound ) {
    ## looks like the output file exists, in which case, if this isn't a speedseq alignment, just start the next step
    chdir( $localdatadir );

    if ( defined ($rnaseq) || ($after eq "rnaseq" )) {
	my $cmd = "PBS_call_GATK_variants_RNAseq.pl \\\n" .
	    "\t--localdatadir $localdatadir \\\n" .
	    "\t--bamFile $outFile \\\n" .
	    "\t--bamidxFile $outIndex \\\n" .
	    "\t--bwaidx $bwaidx_orig \\\n" .
	    "\t--numcpus $numcpus \\\n" .
	    "\t--cpuMem $cpuMem \\\n" .
	    "\t--prefix $prefix \\\n" .
	    "\t--runtype $runtype --submit $submit --after $after";
	$cmd .= " \\\n\t--serapeum $serapeum" if defined $serapeum;
	$cmd .= " \\\n\t--serapeum_path $serapeum_outpath" if defined $serapeum_outpath;

	#$recalibrate_bamFile->addCmd("\t--numcpus $numcpus \\");
	#$recalibrate_bamFile->addCmd("\t--cpuMem $cpuMem \\");
	#$recalibrate_bamFile->addCmd("\t--prefix $prefix \\");
	#$recalibrate_bamFile->addCmd("\t--runtype $runtype --submit $submit --after $after");
    } else {
	## TBD
    }


    #`$cmd`;
    #`$cmd | sed -e \"s:zenodotus=TRUE:zenodotus=TRUE\\nTMPDIR=\$PWD:\"> gen_variant_name_tbd.sh`;
    #`$cmd | sed -e \"s:zenodotus=TRUE:zenodotus=TRUE\\nTMPDIR=$ENV{PWD}:\"> gen_variant_name_tbd.sh`;
    #`$cmd | sed -e \"s:zenodotus=TRUE:zenodotus=TRUE\\nTMPDIR=/scratchLocal/waltmape/lftp_testing/:\"> gen_variant_name_tbd.sh`;
    chdir( "$ENV{PWD}\n" );
} else {
    my $recalibrate_bamFile = PBS->new;
    my $script = "${prefix}_${runtype}_recalibrate_bamFile_run.sh";

    if ($local == 0 ){
	$recalibrate_bamFile->setPlatform("panda");
	$recalibrate_bamFile->setEmail("pwaltman\@rockefeller.edu");
	$recalibrate_bamFile->setWallTime( $walltime );
	$recalibrate_bamFile->setNumCPUs( $numcpus );
	$recalibrate_bamFile->setMemory( "${cpuMem}G" );
	$recalibrate_bamFile->setScriptName($script);
	$recalibrate_bamFile->setName("${prefix}_${runtype}_recalibrate_bamFile");
	$recalibrate_bamFile->setTarget( "zenodotus" );

	if (defined($target)) {
	    $recalibrate_bamFile->setTarget($target);
	}
    }


    ## Setting work directories
    my $execdatadir  = "\$TMPDIR";
    my $oeLabSharedDir = "/zenodotus/dat02/elemento_lab_scratch/oelab_scratch_scratch007/kwe2001";
    my $homedir = "$ENV{HOME}";
    my $localperlscriptsdir = "$oeLabSharedDir/programs/SNPseeqer";
    my $tmpdir = "$execdatadir/tmp";



    ## If the job is run locally
    if ($local == 1) {
	$localdatadir = "$ENV{PWD}"; 
	$execdatadir  = "$ENV{PWD}";
	# Actual start to the bash script
	$recalibrate_bamFile->addCmd("cd $ENV{PWD}");
    }
    else {
	$recalibrate_bamFile->addCmd("cd $execdatadir");
	$recalibrate_bamFile->addCmd("echo \"localdatadir: $localdatadir\"");
	$recalibrate_bamFile->addCmd("echo \"execdatadir: $execdatadir\"");
	$recalibrate_bamFile->addCmd("echo \"node: \$\(uname -n\)\"");
    }
    
    

    #  copy it to remote 
    if ($local == 0) {
	if ( defined $serapeum ) {
	    $recalibrate_bamFile->addCmd("curdir=\$(pwd -P)");
	    $recalibrate_bamFile->addCmd("cd $execdatadir");
	    $recalibrate_bamFile->addCmd("lftp -e \"get $serapeum_path/$bamFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $recalibrate_bamFile->addCmd("lftp -e \"get $serapeum_path/$bamidxFile; bye\" sftp://${serapeum}.qib.pbtech");
	    $recalibrate_bamFile->addCmd("lftp -e \"get $serapeum_path/$recalTable; bye\" sftp://${serapeum}.qib.pbtech");
	    $recalibrate_bamFile->addCmd("cd \$curdir");
	} else {
	    $recalibrate_bamFile->addCmd("rsync -rptgoLv $localdatadir/$bamFile $execdatadir");
	    $recalibrate_bamFile->addCmd("rsync -rptgoLv $localdatadir/$bamidxFile $execdatadir");
	    $recalibrate_bamFile->addCmd("rsync -rptgoLv $localdatadir/$recalTable $execdatadir");
	}
	$recalibrate_bamFile->addCmd("rsync -rptgoLv $bwapath $execdatadir");
	$bwapath = basename( $bwapath );
	$bwaidx = basename( $bwaidx );
	$bwaidx = "$execdatadir/$bwapath/$bwaidx";
    }



    ## generate recal bamFile
    $recalibrate_bamFile->addCmd("if [ -d $tmpdir ]; then");
    $recalibrate_bamFile->addCmd("\t mkdir $tmpdir -p");
    $recalibrate_bamFile->addCmd("fi");
    
    $recalibrate_bamFile->addCmd("echo \"generate recalibrated bamFile: $outFile\"" );
    
    my $RATC_MEM = max( ($numcpus + 8), ($numcpus * $cpuMem)-8);
    my $cmd = "java -Djava.io.tmpdir=$tmpdir \\\n\t";
    $cmd .= "    -Xmx${RATC_MEM}g \\\n";
    $cmd .= "    -jar $gatk \\\n";
    $cmd .= "    -nct $numcpus \\\n";
    $cmd .= "    -T PrintReads \\\n";
    $cmd .= "    -R $bwaidx \\\n";
    $cmd .= "    -I $bamFile \\\n";
    $cmd .= "    -BQSR $recalTable \\\n";
    $cmd .= "    -o $outFile";
    $recalibrate_bamFile->addCmd( $cmd );
    
    
    $recalibrate_bamFile->addCmd("if [ ! -f $outFile ]; then" );
    $recalibrate_bamFile->addCmd("    echo \"$outFile not generated, see log for explanation\" ");
    $recalibrate_bamFile->addCmd("    exit 1" );
    $recalibrate_bamFile->addCmd("fi");


    if ($local == 0) { # get BAM and bam index files back, 
	if ( defined $serapeum ) {
	    ## make sure to check if the outpath exists
	    if ( ! &lftp_filechk( $USER,
				  "",
				  $serapeum_outpath,
				  $serapeum ) ) {
		$recalibrate_bamFile->addCmd("lftp -e \"mkdir -p /$serapeum_root_dir/$USER/$serapeum_outpath; bye\" sftp://${serapeum}.qib.pbtech"); 
	}
	    $recalibrate_bamFile->addCmd("lftp -e \"put $outFile; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
	    $recalibrate_bamFile->addCmd("lftp -e \"put $outIndex; bye\" sftp://${serapeum}.qib.pbtech/$serapeum_root_dir/$USER/$serapeum_outpath"); 
	} else {
	    $recalibrate_bamFile->addCmd("rsync -av $outFile $localdatadir");
	    $recalibrate_bamFile->addCmd("rsync -av $outIndex $localdatadir");
	}
    }



    ## cleanup to do if executing distantly
    $recalibrate_bamFile->addCmd("rm -rf $execdatadir/$bwapath &");                      # erase genome
    $recalibrate_bamFile->addCmd("rm -f $execdatadir/$bamFile &");   # erase data file 1
    $recalibrate_bamFile->addCmd("rm -f $execdatadir/$bamidxFile &");   # erase data file 1
    $recalibrate_bamFile->addCmd("rm -f $execdatadir/$recalTable &");   # erase data file 1


    ## we also should remove the remote copy of the input $bamFile (the realigned bamFile) since it it extraneous at this point, along with the index & recal table files
    if ( defined $serapeum ) {
	$recalibrate_bamFile->addCmd("lftp -e \"rm /$serapeum_root_dir/$USER/$serapeum_outpath/$bamFile; bye\" sftp://${serapeum}.qib.pbtech") if defined $bamFile;         # erase data file 1
	$recalibrate_bamFile->addCmd("lftp -e \"rm /$serapeum_root_dir/$USER/$serapeum_outpath/$bamidxFile; bye\" sftp://${serapeum}.qib.pbtech") if defined $bamidxFile;   # erase data file 2
	$recalibrate_bamFile->addCmd("lftp -e \"rm /$serapeum_root_dir/$USER/$serapeum_outpath/$recalTable; bye\" sftp://${serapeum}.qib.pbtech") if defined $recalTable;   # erase data file 3
    } else {
	if ( $localdatadir ne $execdatadir ) {
	    $recalibrate_bamFile->addCmd("rm -f $localdatadir/$bamFile &") if defined $bamFile;         # erase data file 1
	    $recalibrate_bamFile->addCmd("rm -f $localdatadir/$bamidxFile &") if defined $bamidxFile;   # erase data file 2
	    $recalibrate_bamFile->addCmd("rm -f $localdatadir/$recalTable &") if defined $recalTable;   # erase data file 3
	}
    }


    if ( defined $after ) {
	if ( defined ($rnaseq) || ($after eq "rnaseq" )) {
	    $recalibrate_bamFile->addCmd("cd $localdatadir");
	    $recalibrate_bamFile->addCmd("PBS_call_GATK_variants_RNAseq.pl \\");
	    $recalibrate_bamFile->addCmd("\t--localdatadir $localdatadir \\");
	    $recalibrate_bamFile->addCmd("\t--bamFile $outFile \\");
	    $recalibrate_bamFile->addCmd("\t--bamidxFile $outIndex \\");
	    $recalibrate_bamFile->addCmd("\t--bwaidx $bwaidx_orig \\");
	    $recalibrate_bamFile->addCmd("\t--numcpus $numcpus \\");
	    $recalibrate_bamFile->addCmd("\t--cpuMem $cpuMem \\");
	    $recalibrate_bamFile->addCmd("\t--prefix $prefix \\");
	    $recalibrate_bamFile->addCmd("\t--runtype $runtype --submit $submit --after $after");
	} else {
	    ## TBD
	}
    }

    switch( $submit ) {
	case 0 { $recalibrate_bamFile->print; }
	case 1 { my $job = $recalibrate_bamFile->submit; print "JOBID=$job\n";}
	case 2 { $recalibrate_bamFile->_writeScript(); }
	else { $recalibrate_bamFile->print; }
    }
}



