#!/bin/env perl
use lib "/home/waltmape/elementoLabSoftware/PERL_MODULES";
use strict;
use Sets;
use PBS;
use Getopt::Long;
use Switch;
use File::Basename;
use Data::Dumper;
use POSIX qw/ceil/;
use List::Util qw[min max];

if (@ARGV == 0) {
  print "Args: --bamFile=FILE --bamidxFile=FILE --bwaidx=FILE --submit=INT\n";
  exit;
}

my $submit = 2;
my $localdatadir = "$ENV{PWD}";
my $target   = undef;
my $bamFile = undef;
my $bamidxFile = undef;
my $targetFile = undef;
my $numcpus = 16;
my $cpuMem  = 3;
my $walltime = "36:00:00";
my $after    = undef;
my $debug = 0;
my $gatk = "/home/waltmape/bin/GenomeAnalysisTK.jar";
my $prefix = "test_devel";
my $runtype = "BWAMEM_GATK";
my $bwaidx = "$ENV{HOME}/FAHNSC/resources/grch37/indices/bwa/broad/Homo_sapiens_assembly19.fasta";
##  done below now
##my $bwaidx_orig=$bwaidx;  
##my $bwapath = dirname( $bwaidx );

GetOptions("submit=s"        => \$submit,
	   "localdatadir=s"  => \$localdatadir,
	   "target=s"        => \$target,
           "bamFile=s"       => \$bamFile,
           "bamidxFile=s"    => \$bamidxFile,
	   "numcpus=s"       => \$numcpus,
	   "cpuMem=s"        => \$cpuMem,
           "walltime=s"      => \$walltime,
           "after=s"         => \$after,
	   "debug=s"         => \$debug,
	   "gatk=s"          => \$gatk,
           "prefix=s"        => \$prefix,
           "runtype=s"       => \$runtype,  
	   "bwaidx=s"        => \$bwaidx);

die( "No bam file provided for parameter --bamFile" ) if not defined $bamFile;
die( "No bam index file provided for parameter --bamidxFile" ) if not defined $bamidxFile;
die( "per process memory must be specified in terms of G (with or without the 'G'), NOT megabytes") if $cpuMem =~ /[mbMB]/ ; ## check if cpuMem is specified in terms megabytes
if ( $cpuMem =~ /gG/ ) {
    $cpuMem =~ s/gG//;
}
my $totalMem = ($numcpus * 2) + 8;
die( "per total memory of job (# cpus * memory_per_cpu) must be less than 128G; current total: $totalMem") if $totalMem > 128;

my $jobMem = ceil($totalMem / $numcpus);
if (! -e $gatk    ) {
  die("GATK jar file: $gatk does not exist.\n");
}


my $local = 0;
if ( $submit == 3 ) {
  $local = 1;
}

if (! -e $bamFile) {
  die("$bamFile does not exist.\n");
}
if (! -e $bamidxFile) {
  die("$bamFile does not exist.\n");
}


my $call_GATK_variants = PBS->new;
my $script = "${prefix}_${runtype}_call_GATK_variants_run.sh";
my $bwaidx_orig=$bwaidx;
my $bwapath = dirname( $bwaidx );


if ($local == 0 ){
    $call_GATK_variants->setPlatform("panda");
    $call_GATK_variants->setEmail("pwaltman\@rockefeller.edu");
    $call_GATK_variants->setWallTime( $walltime );
    $call_GATK_variants->setMemory( "${totalMem}G" );
    $call_GATK_variants->setScriptName($script);
    $call_GATK_variants->setName("${prefix}_${runtype}_call_HaplotypeCaller_and_filter");
    $call_GATK_variants->setTarget( "zenodotus" );

    if (defined($target)) {
	$call_GATK_variants->setTarget($target);
    }
}


# Setting work directories


my $execdatadir  = "\$TMPDIR";
my $oeLabSharedDir = "/zenodotus/dat02/elemento_lab_scratch/oelab_scratch_scratch007/kwe2001";
my $homedir = "$ENV{HOME}";

my $localperlscriptsdir = "$oeLabSharedDir/programs/SNPseeqer";

my $tmpdir = "$execdatadir/tmp";


# If the job is run locally
if ($local == 1) {
  $localdatadir = "$ENV{PWD}"; 
  $execdatadir  = "$ENV{PWD}";
  # Actual start to the bash script
  $call_GATK_variants->addCmd("cd $ENV{PWD}");
}
else {
    $call_GATK_variants->addCmd("cd $execdatadir");
    $call_GATK_variants->addCmd("echo \"localdatadir: $localdatadir\"");
    $call_GATK_variants->addCmd("echo \"execdatadir: $execdatadir\"");
    $call_GATK_variants->addCmd("echo \"node: \$\(uname -n\)\"");
}




# file 
#  copy it to remote 
if ($local == 0) {
  $call_GATK_variants->addCmd("rsync -rptgoLv $localdatadir/$bamFile $execdatadir");
  $call_GATK_variants->addCmd("rsync -rptgoLv $localdatadir/$bamidxFile $execdatadir");
  $call_GATK_variants->addCmd("rsync -rptgoLv $bwapath $execdatadir");
  $bwapath = basename( $bwapath );
  $bwaidx = basename( $bwaidx );
  $bwaidx = "$execdatadir/$bwapath/$bwaidx";
}




## generate raw vcf file (single sample
my $outFile="${prefix}_raw.vcf";
$call_GATK_variants->addCmd("if [ -d $tmpdir ]; then");
$call_GATK_variants->addCmd("\t mkdir $tmpdir -p");
$call_GATK_variants->addCmd("fi");

$call_GATK_variants->addCmd("echo \"generate realigned bamFile: $outFile\"" );

my $RATC_MEM = max( (1 + 8), ($totalMem-8));
my $cmd = "java -Djava.io.tmpdir=$tmpdir \\\n\t";
$cmd .= "    -Xmx${RATC_MEM}g \\\n";
$cmd .= "    -jar $gatk \\\n";
$cmd .= "    -T HaplotypeCaller \\\n";
$cmd .= "    -R $bwaidx \\\n";
$cmd .= "    -I $bamFile \\\n";
$cmd .= "    -o $outFile \\\n";
$cmd .= "    -dontUseSoftClippedBases -stand_call_conf 20.0 -stand_emit_conf 20.0";
$call_GATK_variants->addCmd( $cmd );


$call_GATK_variants->addCmd("if [ ! -f $outFile ]; then" );
$call_GATK_variants->addCmd("    echo \"$outFile not generated, see log for explanation\" ");
$call_GATK_variants->addCmd("    exit 1" );
$call_GATK_variants->addCmd("fi");

if ($local == 0) { # get BAM and bam index files back, 
    $call_GATK_variants->addCmd("rsync -av $outFile $localdatadir");
}



## now, do the final filtering step
my $inFile=$outFile;
my $outFile="${prefix}_filtered.vcf";
my $cmd = "java -Djava.io.tmpdir=$tmpdir \\\n\t";
$cmd .= "    -Xmx${RATC_MEM}g \\\n";
$cmd .= "    -jar $gatk \\\n";
$cmd .= "    -T VariantFiltration \\\n";
$cmd .= "    -R $bwaidx \\\n";
$cmd .= "    -V $inFile \\\n";
$cmd .= "    -o $outFile \\\n";
$cmd .= "    -window 35 -cluster 3 -filterName FS -filter \"FS > 30.0\" -filterName QD -filter \"QD < 2.0\"";
$call_GATK_variants->addCmd( $cmd );


$call_GATK_variants->addCmd("if [ ! -f $outFile ]; then" );
$call_GATK_variants->addCmd("    echo \"$outFile not generated, see log for explanation\" ");
$call_GATK_variants->addCmd("    exit 1" );
$call_GATK_variants->addCmd("fi");

if ($local == 0) { # get BAM and bam index files back, 
    $call_GATK_variants->addCmd("rsync -av $outFile $localdatadir");
}


switch( $submit ) {
    case 0 { $call_GATK_variants->print; }
    case 1 { my $job = $call_GATK_variants->submit; print "JOBID=$job\n";}
    case 2 { $call_GATK_variants->_writeScript(); }
    else { $call_GATK_variants->print; }
}
