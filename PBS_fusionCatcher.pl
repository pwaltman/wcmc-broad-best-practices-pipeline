#!/bin/env perl
use lib "/home/waltmape/elementoLabSoftware/PERL_MODULES";
use strict;
use Sets;
use PBS;
use Getopt::Long;
use Switch;
use File::Basename;
use Data::Dumper;

if (@ARGV == 0) {
  print "Args: --batchFile=FILE --normalFile=FILE (optional) --submit=INT (PREFERRED)\n";
  print "OR";
  print "Args: --fastq1=FILE --fastq2=FILE --norm_fastq1=FILE (optional) --norm_fastq2=FILE  (optional) --submit=INT\n";
  exit;
}

my $submit = 0;
my $batchFile = undef;
my $normalFile = undef;
my $fastq1 = undef;
my $fastq2 = undef;
my $norm_fastq1 = undef;
my $norm_fastq2 = undef;
my $numcpus = 16;
my $cpuMem  = 4;
my $walltime = "24:00:00";
my $target   = undef;
my $debug = 0;
my $prefix = "test_devel";
my $sampleName="testSample";
my $libraryName="testLibrary";
my $cmd = "echo hello";

GetOptions("submit=s"   => \$submit,
           "batchFile=s"   => \$batchFile,
           "normalFile=s"   => \$normalFile,
           "fastq1=s"   => \$fastq1,
	   "fastq2=s"   => \$fastq2,
           "norm_fastq1=s"   => \$norm_fastq1,
	   "norm_fastq2=s"   => \$norm_fastq2,
           "numcpus=s"  => \$numcpus,
	   "cpuMem=s"   => \$cpuMem,
           "walltime=s" => \$walltime,
	   "debug=s"    => \$debug,
	   "prefix=s" => \$prefix,
	   "sampleName=s" => \$sampleName, 
	   "libraryName=s" => \$libraryName);
           

if (! defined $batchFile ) {
    die( "No fastq file provided for parameter --fastq1. Must provide valid fastq1 file if not providing a batchFile.\n" ) if not defined $fastq1;
    if (! -e $fastq1) {
	die("$fastq1 does not exist.. Must provide valid fastq1 file if not providing a batchFile.\n");
    }
    die( "No fastq file provided for parameter --fastq2. Must provide valid fastq2 file if not providing a batchFile.\n" ) if not defined $fastq2;
    if (! -e $fastq2) {
	die("$fastq2 does not exist.. Must provide valid fastq2 file if not providing a batchFile.\n");
    }

    if ( defined $norm_fastq1 ) {
	if (! -e $norm_fastq1) {
	    die("$norm_fastq1 does not exist.. Must provide valid norm_fastq1 file if not providing a norm_fastq1.\n");
	}
	if ( ! defined $norm_fastq2 ) {
	    die("norm_fastq2 not specified. Please provide the fastq containing the paired ends for $norm_fastq1.\n");
	}
    }
    if ( defined $norm_fastq2 ) {
	if (! -e $norm_fastq2) {
	    die("$norm_fastq2 does not exist.. Must provide valid norm_fastq2 file if not providing a norm_fastq1.\n");
	}
    }
} else {
    if (! -e $batchFile) {
	die("$batchFile does not exist.. Must provide valid batchFile file if not providing a batchFile.\n");
    }

    if ( defined $normalFile ) {
	if (! -e $normalFile) {
	    die("$normalFile does not exist.. Must provide valid normalFile file if not providing a normalFile.\n");
	}
    }
}
die( "per process memory must be specified in terms of G (with or without the 'G'), NOT megabytes") if $cpuMem =~ /[mbMB]/ ; ## check if cpuMem is specified in terms megabytes
if ( $cpuMem =~ /gG/ ) {
    $cpuMem =~ s/gG//;
}
my $totalMem = $numcpus * $cpuMem;
die( "per total memory of job (# cpus * memory_per_cpu) must be less than 128G; current total: $totalMem") if $totalMem > 128;

my $local = 0;
if ( $submit == 3 ) {
  $local = 1;
}


my %fq1s;
my %fq2s;
my %samps;
my %norm_fq1s;
my %norm_fq2s;
my %norm_samps;
my @elts;
my @fqs;
if ( defined $batchFile ) {
    open( my $IN, "<", $batchFile );
    while (my $line = <$IN>)  {
	chomp $line;
	$line =~ /^#/ and next;
	@elts = split("\t", $line );
	@fqs = split( ",", $elts[0] );

	$fq1s{ $elts[1] } = $fqs[0] ; #push( @fq1s, $fqs[0] );
	$fq2s{ $elts[1] } = $fqs[1]; #push( @fq2s, $fqs[1] );
	#push( @samps, $elts[1] ); 
    }
    close $IN;

    if ( defined $normalFile ) {
	open( my $IN, "<", $normalFile );
	while (my $line = <$IN>)  {
	    chomp $line;
	    $line =~ /^#/ and next;
	    @elts = split("\t", $line );
	    @fqs = split( ",", $elts[0] );
	    
	    $norm_fq1s{ $elts[1] } = $fqs[0] ; #push( @norm_fq1s, $fqs[0] );
	    $norm_fq2s{ $elts[1] } = $fqs[1] ; #push( @norm_fq2s, $fqs[1] );
	    #push( @norm_samps, $elts[1] ); 
	}
	close $IN;
    }

} else {
    $norm_fq1s{ $sampleName } = $fastq1; #push( @fq1s, $fastq1 );
    $norm_fq2s{ $sampleName } = $fastq2; #push( @fq2s, $fastq2 );
    #push( @samps, $sampleName );

    if ( defined $norm_fastq1 ) {
	$norm_fq1s{ $sampleName } = $norm_fastq1; #push( @norm_fq1s, $norm_fastq1 );
	$norm_fq2s{ $sampleName } = $norm_fastq2; #push( @norm_fq2s, $norm_fastq2 );
	#push( @norm_samps, $sampleName );
    }
}


my $fusionCatcher;
my $script;
# Setting work directories
my $localdatadir = "$ENV{PWD}";
my $execdatadir  = "\$TMPDIR";
my $oeLabSharedDir = "/zenodotus/dat02/elemento_lab_scratch/oelab_scratch_scratch007/kwe2001";
my $homedir = "$ENV{HOME}";
my $bindir = "$ENV{BINDIR}";

my $localFCdir = "$bindir/fusioncatcher/latest/bin";
my $localFCDatadir = "$bindir/fusioncatcher/latest/data";
my $localperlscriptsdir = "$oeLabSharedDir/programs/SNPseeqer";

my $errorlog = "${sampleName}_fusionCatcher.log.txt";
my $tmpdir = "$execdatadir/tmp";
my $SortedDedupedIndexBam = "$execdatadir/dedup_sorted_$prefix.bam";

sub debugStmtWrapper {
    my $CMD = shift;
    die "Must specify the command to add to the script" if not defined $CMD;
    my $LOG = shift // $errorlog;
    my $FUSIONCATCHER = shift // $fusionCatcher;
    my $DBG = shift // $debug;

    if($DBG==0){
	$FUSIONCATCHER->addCmd("$CMD");
    }
    else{
	$FUSIONCATCHER->addCmd("$CMD >> $LOG");
	$FUSIONCATCHER->addCmd('if $! -ne 0; then');
	$FUSIONCATCHER->addCmd("Error in command: $CMD" );
	$FUSIONCATCHER->addCmd("exit 1");
	$FUSIONCATCHER->addCmd("else");
	$FUSIONCATCHER->addCmd("rm $LOG");
	$FUSIONCATCHER->addCmd("fi");
    }
}



my @common = ();
if ( defined $normalFile ) {
    foreach (keys %fq1s) {
	push(@common, $_) if exists $norm_fq1s{$_};
    }
} else {
    @common = keys %fq1s; 
}

foreach my $k (@keys) {
    $fusionCatcher = PBS->new;
    $script = "${sampleName}_fusionCatcher_job.sh";

    if ($local == 0 ){
	$fusionCatcher->setPlatform("panda");
	$fusionCatcher->setEmail("pwaltman\@rockefeller.edu");
	$fusionCatcher->setWallTime( $walltime );
	$fusionCatcher->setNumCPUs( $numcpus );
	$fusionCatcher->setMemory( "${cpuMem}G" );
	$fusionCatcher->setScriptName($script);
	$fusionCatcher->setName("${sampleName}_fusionCatcher");
	$fusionCatcher->setTarget( "zenodotus" );
    }


    # If the job is run locally
    if ($local == 1) {
	$localdatadir = "$ENV{PWD}"; 
	$execdatadir  = "$ENV{PWD}";
	# Actual start to the bash script
	$fusionCatcher->addCmd("cd $ENV{PWD}");
    }
    else {
	$fusionCatcher->addCmd("cd $execdatadir");
	$fusionCatcher->addCmd("echo \"$execdatadir\"");
    }

    #  copy fastqs to remote 
    if ($local == 0) {
	$fusionCatcher->addCmd("rsync -rptgoLv $localdatadir/$fastq1 $execdatadir");
	$fusionCatcher->addCmd("rsync -rptgoLv $localdatadir/$fastq2 $execdatadir");
	$fusionCatcher->addCmd("rsync -av $localFCDatadir $execdatadir");
    }

    $cmd="$localFCdir/fusioncatcher -t $SV -o $ofile \\\n\t" .
	"-g $bwaidx \\\n\t";
    $cmd .= "-x \$TMPDIR/${excludeFile} \\\n\t" if defined $excludeFile;
    $cmd .= "\$TMPDIR/${bamFile}";

    &debugStmtWrapper( $cmd,
		       $errorlog,
		       $dellySV,
		       $debug );


#if ($local == 0) { # get BAM and bam idenx files back, 
#    if ( $alignAlg eq "speedseq" ) {
#	$fusionCatcher->addCmd("rsync -av --remove-source-files $execdatadir/${prefix}.bam $localdatadir");
#	$fusionCatcher->addCmd("rsync -av --remove-source-files $execdatadir/${prefix}.bam.bai $localdatadir");
#	$fusionCatcher->addCmd("rsync -av --remove-source-files $execdatadir/${prefix}.discordants.bam $localdatadir");
#	$fusionCatcher->addCmd("rsync -av --remove-source-files $execdatadir/${prefix}.discordants.bam.bai $localdatadir");
#	$fusionCatcher->addCmd("rsync -av --remove-source-files $execdatadir/${prefix}.splitters.bam $localdatadir");
#	$fusionCatcher->addCmd("rsync -av --remove-source-files $execdatadir/${prefix}.splitters.bam.bai $localdatadir");
#}



# cleanup to do if executing distantly
#$fusionCatcher->addCmd("rm -rf $execdatadir/$bwapath &");                      # erase genome
#$fusionCatcher->addCmd("rm -f $execdatadir/$alnOut1 &") if defined $alnOut1;   # erase data file 1
#$fusionCatcher->addCmd("rm -f $execdatadir/$alnOut2 &") if defined $alnOut2;   # erase data file 2


    switch( $submit ) {
	case 0 { $fusionCatcher->print; }
	case 1 { my $job = $fusionCatcher->submit; print "JOBID=$job\n";}
	case 2 { $fusionCatcher->_writeScript(); }
	else { $fusionCatcher->print; }
    }

}


#for ( my $i; $i++; $i < scalar( @fq1s ) ) {
#}






